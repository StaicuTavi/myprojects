<!DOCTYPE html>
<html lang="en">
<head>
    <title>eMAG - Home</title>
    <link rel="stylesheet" href="../css.css">
    <link rel="stylesheet" href="../index-style.css">
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;562;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css">
</head>
<body>
<div class="page">
    <?php include "../parts/2.header.php"; ?>
    <div class="body gray-bg">
        <div class="index-content">
            <div class="container">
                <div class="menu clearFix">
                    <div class="menu-sidebar">
                        <div class="sidebar-text">
                            <ul>
                                <?php
                                    include "functions.php";
                                    $categories = readCSV("docs-csv/category.csv");
//                                    var_dump($categories);
//                                    print_r($categories);
//                                    die;
                                ?>
                                <?php foreach ($categories as $category):?>
                                <li>
                                    <a href="category.page.php?category=<?php echo $category['name']; ?>">
                                        <i class="fa <?php echo $category['icon']; ?> menu-icon"></i>
                                        <span class="menu-element"><?php echo $category['name']; ?></span>
                                    </a>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="menu-banner">
                        <div>
                            <img src="img/home-banner.jpg" />
                        </div>
                    </div>
                </div>
                <div class="index-options">
                    <div class="widget-benefit">
                        <div class="widget-cell">
                            <span class="widget-icon"><i class="fas fa-wallet"></i></span>
                            <span>Finantare si plata</span>
                        </div>
                    </div>
                    <div class="widget-benefit">
                        <div class="widget-cell">
                            <span class="widget-icon"><i class="fas fa-search"></i></span>
                            <span>Cea mai variata gama de produse</span>
                        </div>
                    </div>
                    <div class="widget-benefit">
                        <div class="widget-cell">
                            <span class="widget-icon"><i class="fas fa-box-open"></i></span>
                            <span>Deschiderea coletului la livrare</span>
                        </div>
                    </div>
                    <div class="widget-benefit">
                        <div class="widget-cell">
                            <span class="widget-icon"><i class="fas fa-undo-alt"></i></span>
                            <span>Retur gatuit in 30 de zile</span>
                        </div>
                    </div>
                    <div class="widget-benefit">
                        <div class="widget-cell">
                            <span class="widget-icon"><i class="fas fa-headset"></i></span>
                            <span>Suport 24/7</span>
                        </div>
                    </div>
                </div>
                <div class="clearFix">
                    <div class="cookies-pol float-right">
                        Pentru furnizarea unui serviciu îmbunătățit folosim cookies. Continuarea navigării se consideră acceptare a politicii de cookies
                    </div>
                </div>
            </div>
        </div>
        <div class="orange">
            <div class="container">
                <div class="outlet">
                    <div class="outlet-img">
                        <img src="img/eMAG-outlet.png" />
                    </div>
<!--Outlet recomandations -->
                    <?php include "../parts/outlet-recomandations.php"; ?>
                </div>
            </div>
        </div>
        <div>
            <?php include "../parts/other-clients-also-visited.php"; ?>
        </div>
        <div class="container">
            <img style="width: 100%" src="img/promotion.png" />
        </div>
        <div>
            <?php include "../parts/recently-added-to-favorites.php"; ?>
        </div>
        <div>
            <?php include "../parts/navigation-history.php"; ?>
        </div>
        <div>
            <!--            Marketplace-->
            <?php include "../parts/marketplace.php"; ?>
        </div>
    </div>
    <!--            Ante-footer - app stores buttons-->
    <?php include "../parts/ante-footer.php"; ?>
    <!--    Footer-->
    <?php include "../parts/footer.php"; ?>
</div>
</body>
</html>