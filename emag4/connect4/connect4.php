<html>
<head>
    <title></title>
    <style>
        .dotred {
            height: 90px;
            width: 90px;
            background-color: red;
            border-radius: 50%;
            display: inline-block;
        }

        .dotgreen {
            height: 90px;
            width: 90px;
            background-color: green;
            border-radius: 50%;
            display: inline-block;
        }

        .dotgrey {
            height: 90px;
            width: 90px;
            background-color: lightgrey;
            border-radius: 50%;
            display: inline-block;
        }
    </style>
</head>
<body>
<?php
$connect4Array = [
    [0, 0, 0, 0, 0, 0, 0],  // 0 = grey
    [0, 0, 0, 0, 0, 0, 0],  // 1 = red
    [0, 0, 0, 0, 0, 0, 2],  // 2 = green
    [0, 0, 0, 1, 0, 2, 1],
    [0, 0, 0, 1, 1, 1, 1],
    [0, 0, 0, 2, 2, 1, 2],
];
for ($i = 0; $i < 6; $i++) {
    for ($j = 0; $j < 7; $j++) {
        if ($j < 4) {
            if ($connect4Array[$i][$j] == 1 && $connect4Array[$i][$j + 1] == 1 && $connect4Array[$i][$j + 2] == 1 && $connect4Array[$i][$j + 3] == 1) {
                $winner = 'Player1 on row ' . ($i + 1);
            } elseif ($connect4Array[$i][$j] == 2 && $connect4Array[$i][$j + 1] == 2 && $connect4Array[$i][$j + 2] == 2 && $connect4Array[$i][$j + 3] == 2) {
                $winner = 'Player2 on row' . ' ' . ($i + 1);
            }
        }

        if ($i < 3 && $j < 7) {
            if ($connect4Array[$i][$j] == 1 && $connect4Array[$i + 1][$j] == 1 && $connect4Array[$i + 2][$j] == 1 && $connect4Array[$i + 3][$j] == 1) {
                $winner = 'Player1 on column ' . ($j + 1);
            } elseif ($connect4Array[$i][$j] == 2 && $connect4Array[$i + 1][$j] == 2 && $connect4Array[$i + 2][$j] == 2 && $connect4Array[$i + 3][$j] == 2) {
                $winner = 'Player2 on column ' . ($j + 1);
            }
        }


        if ($i < 3 && $j < 4) {
            if ($connect4Array[$i][$j] == 1 && $connect4Array[$i + 1][$j + 1] == 1 && $connect4Array[$i + 2][$j + 2] == 1 && $connect4Array[$i + 3][$j + 3] == 1) {
                $winner = 'Player1 Diagonal1';
            } elseif ($connect4Array[$i][$j] == 2 && $connect4Array[$i + 1][$j + 1] == 2 && $connect4Array[$i + 2][$j + 2] == 2 && $connect4Array[$i + 3][$j + 3] == 2) {
                $winner = 'Player2 Diagonal1';
            }
        }

        if ($i > 2 && $j > 0 && $i < 6 && $j < 4) {
            if ($connect4Array[$i][$j] == 1 && $connect4Array[$i - 1][$j + 1] == 1 && $connect4Array[$i - 2][$j + 2] == 1 && $connect4Array[$i - 3][$j + 3] == 1) {
                $winner = 'Player1 Diagonal2';
            } elseif ($connect4Array[$i][$j] == 2 && $connect4Array[$i - 1][$j + 1] == 2 && $connect4Array[$i - 2][$j + 2] == 2 && $connect4Array[$i - 3][$j + 3] == 2) {
                $winner = 'Player2 Diagonal2';
            }
        }
    }
}


?>
<table align="center" border="1" width="700">
    <?php foreach ($connect4Array as $row): ?>
        <tr>
            <?php foreach ($row as $value): ?>
                <?php
                if ($value == 0) {
                    $dotColor = 'dotgrey';
                } elseif ($value == 1) {
                    $dotColor = 'dotred';
                } else {
                    $dotColor = 'dotgreen';
                }
                ?>
                <td width="100" height="100" align="center"><span class="<?php echo $dotColor ?>"></span></td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
</table>
<?php if (isset($winner)) {
    echo $winner;
} ?>
</body>
</html>