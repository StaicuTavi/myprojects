<?php
include 'parts/functions.php';
if (!isLoggedin()){
    header("Location: login.php");
    die;
}

$cartId = getUser()->getCart()->id;
$cart = Cart::find($cartId);

echo json_encode($cart->getTotalQuantity());