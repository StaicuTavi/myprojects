<script>
    function setFavoritePage()
    {
        var hrefHeader = document.getElementById('mySecondHref');
        hrefHeader.href = 'parts/account/login.php';
        var href = document.getElementById('myHref');
        href.href = 'parts/account/login.php';
    }

</script>
<?php
include 'parts/functions.php';

$product = Product::find($_GET['product_id']);
$images = $product->getImages();
if(isset($_SESSION['user_id']) && isset($_GET['product_id'])) {
    $favoriteProduct = Favorite::findOneBy(['product_id' => $_GET['product_id'], 'user_id' => $_SESSION['user_id']]);
    if($favoriteProduct && isLoggedin()){
        echo '<BODY onLoad="myFunction(),setHeartColor()">';
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css" integrity="sha384-xxzQGERXS00kBmZW/6qxqJPyxW3UR0BPsL4c8ILaIWXva5kFi7TxkIIaMiKtqV1Q" crossorigin="anonymous">
    <link rel="stylesheet" href="parts/style.css"/>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title><?php echo $product->name; ?></title>
</head>
<body>
    <?php include 'parts/header.php'; ?>
    <div class="product-content container mb-4">
        <div class="row mb-3">
            <?php echo $product->breadcrumbs; ?>
        </div>
        <div class="row">
            <h3><?php echo $product->name; ?></h3>
        </div>
        <div class="row">
            <div class="col-4 text-secondary pl-0">
                Cod produs:<?php echo $product->cod; ?>
            </div>
            <div class="col-5"></div>
            <div class="col-3">
                <div class="row">
                    <div class="col-6 fb-button borderfb">
                        <i class="fab fa-facebook-f"> Share</i>
                    </div>
                    <div class="col-6 check">
                        <input type="checkbox"><span>Compara</span>
                    </div>
                </div>
            </div>
        </div>
        <div><hr></div>
        <div class="row">
            <div class="col-12 col-xl-4 mr-xl-5 pr-0 pl-0 images product-center">
                <div class="product-middle-image">
                    <img style="width:400px ; height:400px;" src="parts/images/<?php echo $product->image; ?>">
                </div>
                <div class="product-small-images">
                    <?php foreach ($images as $image):?>
                        <img src="parts/images/<?php echo $image->image ?>"/>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-12 col-xl-2 middle pr-0 pl-0 prod-info product-center">
                <span style="color:grey">Opinia clientilor:<br/></span>
                <span>&#9733;&#9733;&#9733;&#9733;&#9733;<br/></span>
                <span class="review">Adauga un review</span> | <span class="intrebari">4 intrebari<br/></span>
                <span style="color: grey">Vandut si livrat de:</span> <span><?php echo $product->vendor; ?> <br/></span>
                <span style="color: grey">Ajunge in:</span>Dolj(Filiasi) <span class="schimba">schimba<br/></span>
                <span><hr></span>
                <span>
                    Ridicare personala<br/>
                <span class="ridicare">alege din 17 puncte de ridicare</span>
                </span>
                <span><hr></span>
                <div class="">
                    <div class="icon-delivery">
                        <i class="fas fa-truck-pickup"></i>
                    </div>
                    <div class="livrare">
                        Livrare standard<br/>
                        <span style="color: #4CAF50"> Sambata, 27 Iun. – Luni, 29 Iun.</span>
                    </div>
                    <div class="free">Gratuit</div>
                </div>
                <span style="color: lightgrey"><hr></span>
                <span style="color: grey">Beneficii:</span>
                <div class="">
                    <span"><i class="fas fa-gift"></i></span><span><b>24 puncte</b> prin cardul eMAG-Raiffeisen </span><span><a href="#">detalii</a> </span>
                </div>
                <div class="">
                    <span><i class="fas fa-box-open"></i></span><span>Deschiderea coletului la livrare</span>
                </div>
                <div class="">
                    <span><i class="fas fa-undo"></i></span><span> Retur gratuit in 30 de zile</span>
                </div>
                <span style="color: grey">
                    Alege capacitate SSD:
                </span>
                <div class="">
                    <div class="button-ssd"> 256 GB</div>
                    <div class="button-ssd"> 512 GB</div>
                </div>
            </div>
            <div class="d-none col-xl-2"></div>
            <div class="col-12 col-xl-2 pr-0 pl-0 add-to-cart-favorites product-center">
                <div class="old-price">
                    <?php echo $product->old_price ;?> Lei
                </div>
                <div class="price">
                    <?php echo $product->getFinalPrice();?> Lei
                </div>
                <a id="myHref" href="parts/favorites/process-add-favorites.php?product_id=<?php echo $product->id; ?>" type="button" class="myHref btn btn-outline-primary width-of-buttons"><span id="heart" class="text-secondary">&hearts;</span> Adauga la favorite</a>
                <br />
                <a href="parts/cart/process-add-cart-items.php?product_id=<?php echo $product->id; ?>" type="button" class="btn btn-outline-primary width-of-buttons">Adauga in cos</a>
            </div>
        </div>
    </div>
    <?php include 'parts/footer.html'; ?>
</body>
</html>
