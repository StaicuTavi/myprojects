<!DOCTYPE html>
<html lang="en">
<head>
    <script>
        function setFavoritePage()
        {
            var hrefHeader = document.getElementById('mySecondHref');
            hrefHeader.href = 'parts/account/login.php';
            var href = document.getElementById('myHref');
            href.href = 'parts/account/login.php';
        }
    </script>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css" integrity="sha384-xxzQGERXS00kBmZW/6qxqJPyxW3UR0BPsL4c8ILaIWXva5kFi7TxkIIaMiKtqV1Q" crossorigin="anonymous">
    <link rel="stylesheet" href="parts/style.css"/>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Home</title>
    <?php
        include 'parts/functions.php';

        $categories = Category::findBy(['1'=>'1']);
        $products = Product::findBy(['1'=>'1']);

    ?>
</head>
<body>
    <?php include 'parts/header.php';?>
    <div class="container content">
        <div class="row">
            <div class="col-5 col-xl-3 menu">
                <?php if(isset($categories)):?>
                    <?php foreach ($categories as $category):?>
                        <div class="row mb-2">
                            <div class="col-12 menu-element">
                                <a href="category.php?category_id=<?php echo $category->id; ?>">
                                    <i class="fa <?php echo $category->icon; ?> menu-icon"></i>
                                    <span class=""><?php echo $category->name; ?></span>
                                </a>
                            </div>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
            <div class="col-7 col-xl-9"><img class="content-banner" src="parts/images/banner3.png"/></div>
        </div>
        <div class="row mt-4 widgets">
            <div class="col-12 col-xl-5">
                <div class="row">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-4">
                                <img class="widget-benefits-icon" src="https://s12emagst.akamaized.net/layout/ro/images/db//54/80812.png" alt="icon">
                            </div>
                            <div class="col-8 pt-2">
                                <span class="text">Finantare si plata</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-4">
                                <img class="widget-benefits-icon" src="https://s12emagst.akamaized.net/layout/ro/images/db//54/80811.png" alt="icon">
                            </div>
                            <div class="col-8 pt-2">
                                <span class="text">Cea mai variata gama de produse</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-xl-5">
                <div class="row">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-4">
                                <img class="widget-benefits-icon" src="https://s12emagst.akamaized.net/layout/ro/images/db//54/80813.png" alt="icon">
                            </div>
                            <div class="col-8 pt-2">
                                <span class="text">Deschiderea coletului la livrare</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-4">
                                <img class="widget-benefits-icon" src="https://s12emagst.akamaized.net/layout/ro/images/db//54/80814.png" alt="icon">
                            </div>
                            <div class="col-8 pt-2">
                                <span class="text">Retur gratuit in 30 de zile</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-5 d-xl-none"></div>
            <div class="col-2 col-xl-2">
                <img class="widget-benefits-icon" src="https://s12emagst.akamaized.net/layout/ro/images/db//54/80815.png" alt="icon">
                <span class="text">Suport 24/7</span>
            </div>
            <div class="col-5 d-xl-none"></div>
        </div>
        <div class="row bg">
            <?php if(isset($products)): ?>
                <?php foreach ($products as $key=>$product):?>
                    <?php if($key < 4): ?>
                        <div class="col-6 col-xl-3 product-homepage"><?php product($product); ?></div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <?php include 'parts/footer.html';?>
</body>
</html>