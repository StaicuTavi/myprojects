<?php
include "parts/functions.php";

if (!isLoggedin()){
    header("Location: ../account/login.php");
    die;
}
$user = User::find($_SESSION['user_id']);
$favorites = $user->getFavorites();
$favorites = array_reverse($favorites);


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="parts/style.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css" integrity="sha384-xxzQGERXS00kBmZW/6qxqJPyxW3UR0BPsL4c8ILaIWXva5kFi7TxkIIaMiKtqV1Q" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <title>Favorites</title>
</head>
<body>
    <?php include 'parts/header.php' ?>
    <div class="container">
        <h1>Produse favorite</h1>
        <div>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Produs</th>
                    <th scope="col">Pret</th>
                    <th scope="col">Actiuni</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($favorites as $favorite): ?>
                    <?php $favoriteProduct = Product::find($favorite->product_id);?>
                    <tr>
                        <th scope="row">
                            <a href="product.php?product_id=<?php echo $favoriteProduct->id; ?>">
                                <img width="100" src="parts/images/<?php echo $favoriteProduct->image; ?>" />
                            </a>
                        </th>
                        <td>
                            <a href="product.php?product_id=<?php echo $favoriteProduct->id; ?>">
                                <?php echo $favoriteProduct->name; ?>
                            </a>
                        </td>
                        <td><?php echo $favoriteProduct->getFinalPrice(); ?> RON</td>
                        <td>
                            <a href="parts/favorites/process-delete-favorites.php?id=<?php echo $favorite->id ?>" type="button" class="btn btn-outline-danger">Sterge</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php include 'parts/footer.html' ?>
</body>
</html>
