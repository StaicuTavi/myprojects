<?php

include "parts/functions.php";

if (!isLoggedin()){
    header("Location: ../account/login.php");
    die;
}

$userId = $_SESSION['user_id'];
$cartId = getUser()->getCart()->id;
$cart = Cart::findOneBy(['id'=>$cartId]);
$cartItems = $cart->getCartItems();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="parts/style.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css" integrity="sha384-xxzQGERXS00kBmZW/6qxqJPyxW3UR0BPsL4c8ILaIWXva5kFi7TxkIIaMiKtqV1Q" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script
            src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Shopping Cart</title>
    <script>

    </script>
</head>
<body>
<?php include 'parts/header.php' ?>
<div class="container">
    <h1>Cosul meu de cumparaturi</h1>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Produs</th>
                <th scope="col">Pret</th>
                <th scope="col">Cantitate</th>
                <th scope="col">Sterge</th>
            </tr>
            </thead>
            <tbody>
            <?php if(isset($cartItems)): ?>
                <?php foreach ($cartItems as $cartItem): ?>
                <?php $product = $cartItem->getProduct() ?>
                    <tr>
                        <th scope="row">
                            <a href="product.php?product_id=<?php echo $product->id; ?>">
                                <img width="100" src="parts/images/<?php echo $product->image; ?>" />
                            </a>
                        </th>
                        <td>
                            <a href="product.php?product_id=<?php echo $product->id; ?>">
                                <?php echo $product->name; ?>
                            </a>
                        </td>
                        <td><?php echo $product->getFinalPrice(); ?> RON</td>
                        <td>
                            <div class="row">
                                <div class="col-2 m-0 p-0" style="text-align: right;">
                                    <div class="row">
                                        <div class="col-11">
                                            <button class="btn btn-outline-danger" id="minus<?php echo $product->id; ?>" onclick="modifyValue('minus',<?php echo $product->id; ?>); "> - </button>
                                        </div>
                                        <div class="col-1"></div>
                                    </div>
                                </div>

                                <div class="col-2 m-0 p-0" style="text-align: center;">
                                    <input class="form-control" id="valueQuantity<?php echo $product->id; ?>" type="text" value="<?php echo $cartItem->quantity; ?>" />
                                </div>
                                <div class="col-2 m-0 p-0" style="text-align: right;">
                                    <div class="row">
                                        <div class="col-1"></div>
                                        <div class="col-10">
                                            <button class="btn btn-outline-primary" id="plus<?php echo $product->id; ?>" onclick="modifyValue('plus',<?php echo $product->id; ?>); "> + </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <a href="parts/cart/process-delete-cart-items.php?product_id=<?php echo $product->id; ?>" type="button" class="close" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <div class="alert alert-info" role="alert">
        <span class="font-weight-bold">Pret total: </span>
        <div id="final-price-php"><?php echo $cart->getFinalPrice(); ?> RON</div>
        <div id="final-price-js"></div>
    </div>
</div>
<script>
    function modifyValue(operation, product_id) {
        //console.log(product_id);
        //console.log(operation);
        //console.log("#valueQuantity"+product_id);
        var newValue = parseInt($("#valueQuantity"+product_id).val());
        if (newValue > 0) {
            if (operation === "minus") {
                if (newValue - 1 <= 0) {
                    alert("Nu poti sa pui mai mic de 0")
                }
                else {
                    newValue = newValue - 1;
                }

            }
            else if(operation === "plus"){
                newValue = newValue + 1;
            }
        }
        else if (newValue <= 0) {
            alert("Nu poti sa pui mai mic de 0")
        }
        $("#valueQuantity"+product_id).val(newValue);
        getQuantityValue(product_id);
    }
    $(document).ready(function() {

    });
    function getQuantityValue(product_id) {
        var quantityValue = parseInt($("#valueQuantity"+product_id).val());
        var sum = 0;
        sum += quantityValue;
            $.ajax({
                url: "getQuantity.php",
                method: "POST",
                data: {
                    quantity : quantityValue,
                    id: product_id,
                    sum: sum
                },
                success: function(result) {
                    $("#final-price-php").remove();
                    console.log(result);
                    $("#final-price-js").html(result+" RON");
                    console.log(sum);
                    Quantity();
                },
                error: function(error) {
                    console.log(error);
                }
            })


    };
    function Quantity() {
        $.ajax({
            url: "getTotalCartQuantity.php",
            method: "GET",
            success: function(result) {
                //$( "myCartBadge" ).add( "span" ).addClass( "badge badge-pill badge-info" ).html('1');
                $("#myCartBadge").html(result);
                console.log(result);
                //$("#final-price-js").html(result+" RON");
            },
            error: function(error) {
                console.log(error);
            }
        })


    };
</script>
<?php include 'parts/footer.html';
?>


</body>
</html>
