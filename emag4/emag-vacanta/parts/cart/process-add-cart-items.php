<?php
include "../functions.php";

if (!isLoggedin()){
    header("Location: login.php");
    die;
}

$productId = $_GET['product_id'];
$cartId = getUser()->getCart()->id;

$cartItem = CartItem::findOneBy(['cart_id'=>$cartId, 'product_id'=>$productId]);
$quantity = 1;


if($cartItem){
    $cartItem->quantity++;
} else{
    $cartItem = new CartItem(['product_id'=>$productId, 'cart_id'=>$cartId, 'quantity'=>$quantity]);
}
$cartItem->save();


header("Location:../../Cart.php");
?>
