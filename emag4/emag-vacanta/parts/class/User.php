<?php

/**
 * Created by PhpStorm.
 * User: ionut
 * Date: 8/12/2020
 * Time: 12:51 AM
 */
class User extends BaseTable
{
    public $email;

    public $password;

    public $avatar;

    public $status;

    /** @return Favorite[] */
    public function getFavorites()
    {
        return Favorite::findBy(['user_id'=>$this->id]);
    }

    public static function getTable()
    {
        return 'users';
    }

    public function getCart(){
        $cart = Cart::findOneBy(['user_id'=>$this->id]);
        if (!$cart){
            $cart = new Cart(['user_id'=>$this->id]);
            $cart->save();
        }

        return $cart;

    }
}