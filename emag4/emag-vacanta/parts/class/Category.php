<?php

/**
 * Created by PhpStorm.
 * User: ionut
 * Date: 8/11/2020
 * Time: 5:55 PM
 */
class Category extends BaseTable
{
    public $name;

    public $icon;

    public $discount;


    public function getProducts(){
        return Product::findBy(['category_id'=>$this->id]);
    }

    public static function getTable()
    {
        return 'categories';
    }


}