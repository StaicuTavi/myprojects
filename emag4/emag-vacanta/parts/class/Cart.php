<?php

/**
 * Created by PhpStorm.
 * User: ionut
 * Date: 8/23/2020
 * Time: 2:30 AM
 */
class Cart extends BaseTable
{
    public $user_id;

    public $address_id;

    public $price;

    public function getCartItems(){
        return CartItem::findBy(['cart_id'=>$this->id]);
    }

    public static function getTable()
    {
        return 'carts';
    }

    public function getFinalPrice(){
        $price = 0;
        foreach ($this->getCartItems() as $cartItem){
            $price += $cartItem->getPrice();
            if($price > 0.01){
                $price = ceil($price) - 0.01;
            }
        }

        return $price;
    }

    public function getTotalQuantity(){
        $quantity = 0;
        foreach ($this->getCartItems() as $cartItem){
            $quantity += $cartItem->quantity;
        }

        return $quantity;
    }




}