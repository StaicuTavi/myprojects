<?php
session_start();
$mysqli = mysqli_connect("127.0.0.1", "root", "scoalait123", "web-05-tavi");


include "class/BaseTable.php";
include "class/Product.php";
include "class/User.php";
include "class/Address.php";
include "class/Favorite.php";
include "class/Category.php";
include "class/Cart.php";
include "class/CartItem.php";
include "class/Image.php";

function readMySQL($table){
    global $mysqli;
    $query = mysqli_query($mysqli, 'SELECT * FROM `'.$table.'`;'); 

    return $query->fetch_all(MYSQLI_ASSOC);
}

function readCSV($fileName)
{
    $fileLines = file($fileName);
    $data = [];
    $header = str_getcsv($fileLines[0]);
    unset($fileLines[0]);

    foreach ($fileLines as $fileLine) {
        $data[] = array_combine($header, str_getcsv($fileLine));
    }

    return $data;
}

function search($items, $key, $value)
{
    $result = [];
    foreach ($items as $item){
        if ($value == $item[$key]){
            $result[]=$item;
        }
    }

    return $result;
}

function searchMySQL($table, $column, $value)
{
    global $mysqli;
    $query = mysqli_query($mysqli, 'SELECT * FROM `'.$table.'` WHERE '.$column.'="'.$value.'";');

    return $query->fetch_all(MYSQLI_ASSOC);
}

function delete($table, $id)
{
    global $mysqli;
    $query = mysqli_query($mysqli, "DELETE FROM `$table` WHERE id=".intval($id));
}

function insert($table, $data)
{
    global $mysqli;
    $columns =[];
    $values =[];
    foreach ($data as $column=>$value){
        $columns[]=mysqli_real_escape_string($mysqli, $column);
        $values[]=mysqli_real_escape_string($mysqli, $value);
    }
    $columnsList = implode('`, `', $columns);
    $valuesList = implode("','", $values);

    $query = mysqli_query($mysqli, "INSERT INTO `$table` (`$columnsList`) VALUES ('$valuesList');");

    return mysqli_insert_id($mysqli);
}

function update($table, $data, $id)
{
    global $mysqli;
    $sets = [];
    foreach ($data as $column=>$value){
        $sets[]=mysqli_real_escape_string($mysqli, $column)."`='".mysqli_real_escape_string($mysqli, $value);
    }
    $setsList = implode("',`", $sets);

    $query = mysqli_query($mysqli, "UPDATE `$table` SET `$setsList' WHERE id=".intval($id).";");
}

function isLoggedin(){
    return isset($_SESSION['user_id']);
}

/**
 * @return User
 */
function getUser(){
    if (isLoggedin()){
        return User::find($_SESSION['user_id']);
    } else {
        $user = new User(['email'=>session_id()]);
        $user->save();
        $_SESSION['user_id'] = $user->id;
        return $user;
    }
}

function product($productObject){
    ?>
    <div class="product margin-top">
        <div class="text-center">
            <img class="product-image" src="parts/images/<?php echo $productObject->image; ?>" />
        </div>
        <div class="mb-0 pb-0">
            <?php
            $star = (int)($productObject->stars);
            ?>
            <p class="text-center mb-0 pb-0">
                <?php
                for($i = 0; $i< $star;$i++){
                    echo "&#11088;";
                }
                ?>
            </p>
            <p class="text-center">66 de review-uri</p>
        </div>
        <div class="mb-0 pb-0">
            <p class="text-center mb-0 pb-0 font-weight-bold">
                <?php echo $productObject->name; ?>
            </p>
            <p class="text-center mb-0 pb-0">în stoc</p>
            <p class="mb-0 pb-0 text-center">
                <span class="">Livrat de</span> <?php echo $productObject->vendor;?>
            </p>
            <p class="text-center mb-0 pb-0 text-danger"><s><?php echo $productObject->old_price; ?> <span>Lei</span></s><span
                    class="">(-41%)
                </span></p>
            <p class="text-center mb-0 pb-0"><?php echo $productObject->getFinalPrice(); ?> <span>Lei</span></p>
            <p class="text-center"><button type="submit" class="btn btn-primary"><i class=""></i>Adauga in Cos</button></p>
        </div>
    </div>
    <?php
}
?>