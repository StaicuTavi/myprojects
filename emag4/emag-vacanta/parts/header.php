<?php
    if(isset($_SESSION['user_id'])):
        $user = User::find($_SESSION['user_id']);
        $cartId = getUser()->getCart()->id;
        $cart = Cart::find($cartId);
?>
<script>
    function setFavoritePage()
    {
        var hrefHeader = document.getElementById('mySecondHref');
        hrefHeader.href = 'parts/account/login.php';
        var href = document.getElementById('myHref');
        href.href = 'parts/account/login.php';
    }
    function myFunction()
    {
        var x = document.getElementById("myDIV");
        x.querySelectorAll("span")[0].style.display = "none";
        var image = document.createElement("img");
        image.setAttribute("onerror", "this.onerror=null");
        image.setAttribute("this.src", "parts/account/avatars/Default.png");
        var a = document.createElement('a');
        var linkText = document.createTextNode("Logout");
        a.appendChild(linkText);
        a.href = "parts/account/process-logout.php";
        image.setAttribute("src", "parts/account/avatars/<?php echo $user->avatar;?>");
        image.setAttribute("height", "25");
        image.setAttribute("width", "25");
        image.setAttribute("border", "1px");
        image.setAttribute("style", "border-radius:80%");
        //image.setAttribute("style", "margin-right:5px");
        x.appendChild(image);
        x.appendChild(a);
        a.setAttribute("style", "margin-left:5px");
        var favorites = document.getElementById('favorites');
        var myBadgeSpan = document.createElement("SPAN");
        myBadgeSpan.setAttribute('class', 'badge badge-pill badge-info');
        myBadgeSpan.innerHTML  = '<?php echo count(Favorite::findBy(['user_id'=>$_SESSION['user_id']])); ?>';
        favorites.appendChild(myBadgeSpan);

        /*var cart = document.getElementById('cart');
        var myBadgeSpan2 = document.createElement("SPAN");
        myBadgeSpan2.setAttribute('class', 'badge badge-pill badge-info');
        myBadgeSpan2.innerHTML  = '/*?php echo count(CartItem::findBy(['cart_id'=>$cartId])); ?>';
        cart.appendChild(myBadgeSpan2);*/
    }
    function setHeartColor()
    {
        var heartColor = document.getElementById('heart');
        heartColor.classList.remove('text-secondary');
        heartColor.classList.add('text-danger');

    }
</script>
<?php
endif;
if (isLoggedin()){
    echo '<BODY onLoad="myFunction()">';
}else{
    echo '<BODY onLoad="setFavoritePage()">';
}
?>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-6 col-xl-2"><a href="emag.php"><img src="parts/images/logo.svg" height="36"/></a></div>
            <div class="col-6 col-xl-4 search-bar">
                <form>
                    <input class="corner" type="text" placeholder="Ai libertatea sa alegi ce vrei" name="search">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                        <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
                    </svg>
                </form>
            </div>
            <div id="myDIV" class="col-4 col-xl-2">
                <span><a href="parts/account/login.php"><i class="far fa-user"></i>
                    Contul meu</a></span>
            </div>
            <div id="favorites" class="myHref col-4 col-xl-2">
                <a id="mySecondHref" href="favorites.php"> <i class="far fa-heart"></i>
                    Favorite</a>
            </div>
            <div class="col-4 col-xl-2">
                <a href="Cart.php"> <i class="fas fa-shopping-cart"></i>
                Cosul meu</a><span class='badge badge-pill badge-info' id="myCartBadge" >
                    <div>
                    <?php
                        if(isset($cart)){
                            echo $cart->getTotalQuantity();
                        }
                    ?>
                    </div>
                </span>
            </div>
        </div>
    </div>
    <div class="container-fluid nav-bar">
        <div class="container">
            <div class="row mt-3 pb-0">
                <nav class="navbar navbar-expand-lg text-white navbar-light">
                    <a class="navbar-brand" href="#"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav text-white">
                            <li class="nav-item text-white">
                                <a class="nav-link text-light" href="#">Produse</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-light" href="#">Genius | un serviciu premium eMAG</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-light" href="#">Doneaza pentru linia intai</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-light" href="#" >Resigilate</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-light" href="#" >Necesare zi de zi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-light" href="#" >Extra-reducerile momentului</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-light" href="#" >Outlet</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-light" href="#" >
                                    <i class="fas fa-headphones"></i>
                                    eMAG Help
                                </a>
                            </li>
                            <li class="nav-item no-border">
                                <a class="nav-link disabled " href="#" tabindex="-1" aria-disabled="true"></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12 pr-0 pl-0">
                <img class="banner" src="parts/images/banner.png"/>
            </div>
        </div>
    </div>
</div>
