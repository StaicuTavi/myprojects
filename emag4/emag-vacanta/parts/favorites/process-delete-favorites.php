<?php
include "../functions.php";

if (!isLoggedin()){
    header("Location: login.php");
    die;
}

$favoriteProduct = Favorite::findOneBy(['id'=>$_GET['id'], 'user_id'=>$_SESSION['user_id']]);

if (!$favoriteProduct){
    die('Nu ai voie!!!!');
}


delete('favorites', $_GET['id']);

header("Location:../../favorites.php");
?>
