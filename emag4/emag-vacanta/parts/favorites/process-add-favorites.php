<?php
include "../functions.php";

if (!isLoggedin()){
    header("Location: login.php");
    die;
}

if (!Favorite::findOneBy(['user_id'=>$_SESSION['user_id'],'product_id'=>$_GET['product_id']])){
    insert('favorites', ['user_id'=>$_SESSION['user_id'], 'product_id'=>$_GET['product_id']]);
}

header("Location:../../favorites.php");
?>
