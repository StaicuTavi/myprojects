<?php

include 'parts/functions.php';

if (!isLoggedin()){
    header("Location: login.php");
    die;
}

$cartId = getUser()->getCart()->id;
$quantity = $_POST["quantity"];
$productId = $_POST["id"];

$cartItem = CartItem::findOneBy(['cart_id'=>$cartId, 'product_id'=>$productId]);
$cartItem->quantity = $quantity;
$cartItem->save();

$cartId = getUser()->getCart()->id;
$cart = Cart::find($cartId);
//sleep(1);
echo json_encode($cart->getFinalPrice());

