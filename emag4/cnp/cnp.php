<td>
    <?php
        //1 95 11 18 16 202 0
        $sex = (int)($cnp/pow(10,12));
        if($sex == 1 || $sex == 2) {
            $birthYear = 1900 + (int)($cnp / pow(10, 10)) % pow(10, 2);
        }else{
            $birthYear = 2000 + (int)($cnp / pow(10, 10)) % pow(10, 2);
        }
        $birthMonth = (int)($cnp/pow(10,8))%pow(10,2);
        $birthDay = (int)($cnp/pow(10,6))%pow(10,2);
        $countyNr = (int)($cnp/pow(10,4))%pow(10,2);
        $year = (int)(date('Y'));
        $month = (int)(date('m'));
        $day = (int)(date('d'));
        if($birthYear > (int)(date('Y'))){
            $age = 'Invalid';
            $birthYear = 'Invalid';
            $birthMonthecho = '';
            $countyNrecho = 'Invalid';
            $gen = 'Invalid';
            $birthDay = '';
        }else {

            if ($month < $birthMonth) {
                $age = $year - $birthYear - 1;
            } elseif ($month == $birthMonth) {
                if ($day < $birthDay) {
                    $age = $year - $birthYear - 1;
                } else {
                    $age = $year - $birthYear;
                }
            } else {
                $age = $year - $birthYear;
            }

            if ($sex == 1 || $sex == 5) {
                $gen = 'Masculin';
            } elseif ($sex == 2 || $sex == 6) {
                $gen = 'Feminin';
            } else {
                $gen = 'Format CNP invalid, indicativul de sex nu are o valoare cunoscuta.';
                die('Format CNP invalid, indicativul de sex nu are o valoare cunoscuta.');
            }

            if ($birthMonth == 1) {
                $birthMonthecho = 'Ianuarie';
            } elseif ($birthMonth == 2) {
                $birthMonthecho = 'Februarie';
            } elseif ($birthMonth == 3) {
                $birthMonthecho = 'Martie';
            } elseif ($birthMonth == 4) {
                $birthMonthecho = 'Aprilie';
            } elseif ($birthMonth == 5) {
                $birthMonthecho = 'Mai';
            } elseif ($birthMonth == 6) {
                $birthMonthecho = 'Iunie';
            } elseif ($birthMonth == 7) {
                $birthMonthecho = 'Iulie';
            } elseif ($birthMonth == 8) {
                $birthMonthecho = 'August';
            } elseif ($birthMonth == 9) {
                $birthMonthecho = 'Septembire';
            } elseif ($birthMonth == 10) {
                $birthMonthecho = 'Octombrie';
            } elseif ($birthMonth == 11) {
                $birthMonthecho = 'Noiembrie';
            } elseif ($birthMonth == 12) {
                $birthMonthecho = 'Decembrie';
            } else {
                $birthMonthecho = 'Format CNP invalid, numarul corespunzator lunii nasterii nu este corect';
            }

            if ($birthDay > 31) {
                $birthDay = 'Format CNP invalid, numarul corespunzator zilei de nastere nu este corect';
            }
            if ($countyNr <= 52) {
                if ($countyNr == 1) {
                    $countyNrecho = 'Alba';
                } elseif ($countyNr == 2) {
                    $countyNrecho = 'Arad';
                } elseif ($countyNr == 3) {
                    $countyNrecho = 'Arges';
                } elseif ($countyNr == 4) {
                    $countyNrecho = 'Bacau';
                } elseif ($countyNr == 5) {
                    $countyNrecho = 'Bihor';
                } elseif ($countyNr == 6) {
                    $countyNrecho = 'Bistrita-Nasaud';
                } elseif ($countyNr == 7) {
                    $countyNrecho = 'Botosani';
                } elseif ($countyNr == 8) {
                    $countyNrecho = 'Brasov';
                } elseif ($countyNr == 9) {
                    $countyNrecho = 'Braila';
                } elseif ($countyNr == 10) {
                    $countyNrecho = 'Buzau';
                } elseif ($countyNr == 11) {
                    $countyNrecho = 'Caras-Severin';
                } elseif ($countyNr == 12) {
                    $countyNrecho = 'Cluj';
                } elseif ($countyNr == 13) {
                    $countyNrecho = 'Constanta';
                } elseif ($countyNr == 14) {
                    $countyNrecho = 'Covasna';
                } elseif ($countyNr == 15) {
                    $countyNrecho = 'Dambovita';
                } elseif ($countyNr == 16) {
                    $countyNrecho = 'Dolj';
                } elseif ($countyNr == 17) {
                    $countyNrecho = 'Galati';
                } elseif ($countyNr == 18) {
                    $countyNrecho = 'Gorj';
                } elseif ($countyNr == 19) {
                    $countyNrecho = 'Harghita';
                } elseif ($countyNr == 20) {
                    $countyNrecho = 'Hunedoara';
                } elseif ($countyNr == 21) {
                    $countyNrecho = 'Ialomita';
                } elseif ($countyNr == 22) {
                    $countyNrecho = 'Iasi';
                } elseif ($countyNr == 23) {
                    $countyNrecho = 'Ilfov';
                } elseif ($countyNr == 24) {
                    $countyNrecho = 'Maramures';
                } elseif ($countyNr == 25) {
                    $countyNrecho = 'Mehedinti';
                } elseif ($countyNr == 26) {
                    $countyNrecho = 'Mures';
                } elseif ($countyNr == 27) {
                    $countyNrecho = 'Neamt';
                } elseif ($countyNr == 28) {
                    $countyNrecho = 'Olt';
                } elseif ($countyNr == 29) {
                    $countyNrecho = 'Prahova';
                } elseif ($countyNr == 30) {
                    $countyNrecho = 'Satu Mare';
                } elseif ($countyNr == 31) {
                    $countyNrecho = 'Salaj';
                } elseif ($countyNr == 32) {
                    $countyNrecho = 'Sibiu';
                } elseif ($countyNr == 33) {
                    $countyNrecho = 'Suceava';
                } elseif ($countyNr == 34) {
                    $countyNrecho = 'Teleorman';
                } elseif ($countyNr == 35) {
                    $countyNrecho = 'Timis';
                } elseif ($countyNr == 36) {
                    $countyNrecho = 'Tulcea';
                } elseif ($countyNr == 37) {
                    $countyNrecho = 'Vaslui';
                } elseif ($countyNr == 38) {
                    $countyNrecho = 'Valcea';
                } elseif ($countyNr == 39) {
                    $countyNrecho = 'Vrancea';
                } elseif ($countyNr == 40) {
                    $countyNrecho = 'Bucuresti';
                } elseif ($countyNr == 41) {
                    $countyNrecho = 'Bucuresti-Sector 1';
                } elseif ($countyNr == 42) {
                    $countyNrecho = 'Bucuresti-Sector 2';
                } elseif ($countyNr == 43) {
                    $countyNrecho = 'Bucuresti-Sector 3';
                } elseif ($countyNr == 44) {
                    $countyNrecho = 'Bucuresti-Sector 4';
                } elseif ($countyNr == 45) {
                    $countyNrecho = 'Bucuresti-Sector 5';
                } elseif ($countyNr == 46) {
                    $countyNrecho = 'Bucuresti-Sector 6';
                } elseif ($countyNr == 51) {
                    $countyNrecho = 'Calarasi';
                } elseif ($countyNr == 52) {
                    $countyNrecho = 'Giurgiu';
                } else {
                    $countyNrecho = 'Format CNP invalid, indicativul de judet nu are o valoare cunoscuta.';
                }
            } else {
                $countyNrecho = 'Format CNP invalid, indicativul de judet nu are o valoare cunoscuta.';
            }
        }

    ?>
        <p>CNP:<?php echo $cnp; ?></p>
        <p>Gen:<?php echo $gen; ?></p>
        <p>Data nasterii:<?php echo $birthDay.'&nbsp;'.$birthMonthecho.'&nbsp;'.$birthYear;?></p>
        <p>Judet:<?php echo $countyNrecho;?></p>
        <p>Varsta:<?php echo $age;?></p>
</td>



