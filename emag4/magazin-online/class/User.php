<?php

/**
 * Created by PhpStorm.
 * User: ionut
 * Date: 8/12/2020
 * Time: 12:51 AM
 */
class User extends BaseTable
{
    public $id;

    public $email;

    public $password;

    public $avatar;

    public $status;

    /**
     * User constructor.
     * @param $id
     * @param $email
     * @param $password
     * @param $avatar
     * @param $status
     */
    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->email = $data['email'];
        $this->password = $data['password'];
        $this->avatar = $data['avatar'];
        $this->status = $data['status'];
    }
    /** @return Favorite[] */
    public function getFavorites()
    {
        return Favorite::findBy(['user_id'=>$this->id]);
    }

    public static function getTable()
    {
        return 'users';
    }

}