<?php

/**
 * Created by PhpStorm.
 * User: ionut
 * Date: 8/12/2020
 * Time: 12:06 AM
 */
class Image extends BaseTable
{
    public $id;

    public $product_id;

    public $image;

    /**
     * Image constructor.
     * @param $id
     * @param $product_id
     * @param $image
     */
    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->product_id = $data['product_id'];
        $this->image = $data['image'];
    }

    public static function getTable()
    {
        return 'images';
    }

}