<?php

/**
 * Created by PhpStorm.
 * User: ionut
 * Date: 8/11/2020
 * Time: 5:55 PM
 */
class Product extends BaseTable
{
    public $id;

    public $category_id;

    public $name;

    public $image;

    public $old_price;

    public $vendor;

    public $stars;

    public $cod;

    public $breadcrumbs;

    public $discount;

    /**
     * Product constructor.
     * @param $id
     * @param Category $category
     * @param $name
     * @param $image
     * @param $old_price
     * @param $vendor
     * @param $stars
     * @param $cod
     * @param $breadcrumbs
     */
    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->category_id = $data['category_id'];
        $this->name = $data['name'];
        $this->image = $data['image'];
        $this->old_price = $data['old_price'];
        $this->vendor = $data['vendor'];
        $this->stars = $data['stars'];
        $this->cod = $data['cod'];
        $this->breadcrumbs = $data['breadcrumbs'];
    }


    public function getFinalPrice()
    {
        return ceil($this->old_price*((100-$this->discount)/100))-0.01;
    }

    /** @return Category */
    public function getCategory()
    {
        return Category::find($this->category_id);
    }

    /** @return Image[] */
    public function getImages()
    {
        return Image::findBy(['product_id'=>$this->id]);;
    }

    public static function getTable()
    {
        return 'products';
    }


}