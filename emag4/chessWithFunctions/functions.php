<?php
    function checkPieceColor($row){
        global $pieceColor ;
        if ($row==0 || $row==1){
            $pieceColor = 'white';
        } else {
            $pieceColor = 'black';
        }
        //return $pieceColor;
    }
    function checkCellColor($currentCell){
        global $cellColor;
        if ($currentCell % 2 == 0){
            $cellColor='grey';
        } else {
            $cellColor='lightgrey';
        }
        //return $cellColor;
    }
    function addPieceInBoard($row, $col, $chessPieces){
        global $chessPiece;
        if ($row == 1 || $row == 6){
            $chessPiece = '&#9817;';
        }elseif ($row == 0 || $row == 7){
            $chessPiece = $chessPieces[$col];
        }else {
            $chessPiece = '&nbsp;';
        }
    }
?>

