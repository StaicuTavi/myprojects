<html>
    <head>
        <title>Chess Table</title>
        <script src="https://kit.fontawesome.com/e04b2906d0.js" crossorigin="anonymous"></script>
        <style>
            body{
                background-image: url("bg2.jpg");
                background-size: cover;
            }
            .eventd{
                text-align: center;
                height: 60px;
                width:60px;
                background-color: #FEFFFF;
            }
            .oddtd{
                text-align:center;
                height:60px;
                width:60px;
                background-color: #000000;
            }
            .wpieces{
                color: #D8A681;
            }
            .bpieces{
                color:#654321;
            }
            .center {
                margin-top: 9%;
                margin-bottom: 9%;
            }
        </style>
    </head>
    <body>
        <table class="center" align="center" valign="middle" width="480px" cellspacing="0px" cellpadding="0px" border="1px">
            <?php
                $piecesArray = ["\"fas fa-chess-king fa-3x\"","\"fas fa-chess-queen fa-3x\"","\"fas fa-chess-rook fa-3x\"","\"fas fa-chess-bishop fa-3x\"","\"fas fa-chess-knight fa-3x\"","\"fa fa-chess-pawn fa-3x\""];
                for ($i = 0; $i < 8; $i++){
                    echo "<tr>";
                    for ($j = 0; $j < 8; $j++){
                        $total = $i + $j;
                        if ($i == 0 && $j == 0){
                            echo "<td class='eventd wpieces'><i class=$piecesArray[2]></i></td>";
                        }elseif ($i == 0 && $j == 1){
                            echo "<td class='oddtd wpieces'><i class=$piecesArray[4]></i></td>";
                        }elseif ($i == 0 && $j == 2){
                            echo "<td class='eventd wpieces'><i class=$piecesArray[3]></i></td>";
                        }elseif ($i == 0 && $j == 3){
                            echo "<td class='oddtd wpieces'><i class=$piecesArray[1]></i></td>";
                        }elseif ($i == 0 && $j == 4){
                            echo "<td class='eventd wpieces'><i class=$piecesArray[0]></i></td>";
                        }elseif ($i == 0 && $j == 5){
                            echo "<td class='oddtd wpieces'><i class=$piecesArray[3]></i></td>";
                        }elseif ($i == 0 && $j == 6){
                            echo "<td class='eventd wpieces'><i class=$piecesArray[4]></i></td>";
                        }elseif ($i == 0 && $j == 7){
                            echo "<td class='oddtd wpieces'><i class=$piecesArray[2]></i></td>";
                        }elseif ($i == 1){
                            if($total%2 == 0){
                                echo "<td class='eventd wpieces'><i class=$piecesArray[5]></i></td>";
                            }else{
                                echo "<td class='oddtd wpieces'><i class=$piecesArray[5]></i></td>";
                            }
                        }elseif ($i == 6){
                            if($total%2 == 0){
                                echo "<td class='eventd bpieces'><i class=$piecesArray[5]></i></td>";
                            }else{
                                echo "<td class='oddtd bpieces'><i class=$piecesArray[5]></i></td>";
                            }
                        }elseif ($i == 7 && $j == 0){
                            echo "<td class='oddtd bpieces'><i class=$piecesArray[2]></i></td>";
                        }elseif ($i == 7 && $j == 1){
                            echo "<td class='eventd bpieces'><i class=$piecesArray[4]></i></td>";
                        }elseif ($i == 7 && $j == 2){
                            echo "<td class='oddtd bpieces'><i class=$piecesArray[3]></i></td>";
                        }elseif ($i == 7 && $j == 3){
                            echo "<td class='eventd bpieces'><i class=$piecesArray[1]></i></td>";
                        }elseif ($i == 7 && $j == 4){
                            echo "<td class='oddtd bpieces'><i class=$piecesArray[0]></i></td>";
                        }elseif ($i == 7 && $j == 5){
                            echo "<td class='eventd bpieces'><i class=$piecesArray[3]></i></td>";
                        }elseif ($i == 7 && $j == 6){
                            echo "<td class='oddtd bpieces'><i class=$piecesArray[4]></i></td>";
                        }elseif ($i == 7 && $j == 7){
                            echo "<td class='eventd bpieces'><i class=$piecesArray[2]></i></td>";
                        }elseif ($total%2==0){
                            echo "<td class='eventd bpieces'></td>";
                        }else{
                            echo "<td class='oddtd bpieces'></td>";
                        }
                    }
                    echo "</tr>";
                }
            ?>
        </table>
    </body>
</html>