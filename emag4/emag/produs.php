<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="styles/category-style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css" integrity="sha384-xxzQGERXS00kBmZW/6qxqJPyxW3UR0BPsL4c8ILaIWXva5kFi7TxkIIaMiKtqV1Q" crossorigin="anonymous">
</head>
<body>
<?php
    include 'functions/function.php';
    $products = readCSV('CSV/products.csv');
    $imgs = readCSV('CSV/images.csv');
    $productName = $_GET['nume'];
    $productID = $_GET['productId'];
    $recommendedProducts = readCSV('CSV/recommended.csv');
?>
<div class="container">
    <?php
        $head = header_funct();
        echo $head;
    ?>
    <?php foreach($products as $product):?>
    <?php if($product['name'] == $productName):?>
    <div class="product-content">
        <div class="product-breadcrumbs">
            <?php
                echo $product['breadcrumbs'];
            ?>
        </div>
        <div class="product-name">
            <h2>
                <?php
                    echo $product['name'];
                ?>
            </h2>
        </div>
        <div class="product-code-buttons clearfix">
            <div class="product-code">
                Cod produs:<?php
                    echo $product['cod'];
                ?>
            </div>
            <div class="right">
                <div class="check">
                    <input type="checkbox"><span>Compara</span>
                </div>
                <div class="fb-button borderfb">
                    <i class="fab fa-facebook-f"> Share</i>
                </div>
            </div>
        </div>
        <div><hr></div>
        <div class="product-middle clearfix">
            <div class="product-img-info-price clearfix">
                <div class="product-images">
                    <div class="product-middle-image clearfix">
                        <img style="width:400px ; height:400px;" src="images/<?php echo $product['image']; ?>">
                    </div>
                    <div class="product-small-images">
                        <?php foreach ($imgs as $img):?>
                            <?php if ($img['id'] == $productID ): ?>
                                <img src="images/<?php echo $img['image'] ?>"/>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="prod-info">
                    <span style="color:grey">Opinia clientilor:<br/></span>
                    <span>&#9733;&#9733;&#9733;&#9733;&#9733;<br/></span>
                    <span class="review">Adauga un review</span> | <span class="intrebari">4 intrebari<br/></span>
                    <span style="color: grey">Vandut si livrat de:</span> <span><?php echo $product['vendor'];?> <br/></span>
                    <span style="color: grey">Ajunge in:</span>Dolj(Filiasi) <span class="schimba">schimba<br/></span>
                    <span><hr></span>
                    <span>
                        Ridicare personala<br/>
                    <span class="ridicare">alege din 17 puncte de ridicare</span>
                    </span>
                    <span><hr></span>
                    <div class="clearfix">
                        <div class="icon-delivery">
                            <i class="fas fa-truck-pickup"></i>
                        </div>
                        <div class="livrare">
                            Livrare standard<br/>
                            <span style="color: #4CAF50"> Sambata, 27 Iun. – Luni, 29 Iun.</span>
                        </div>
                        <div class="free">Gratuit</div>
                    </div>
                    <span style="color: lightgrey"><hr></span>
                    <span style="color: grey">Beneficii:</span>
                    <div class="clearfix">
                        <span style="float: left"><i class="fas fa-gift"></i></span><span style="float: left"><b>24 puncte</b> prin cardul eMAG-Raiffeisen </span><span style="float: left;"><a href="#">detalii</a> </span>
                    </div>
                    <div class="clearfix">
                        <span style="float: left"><i class="fas fa-box-open"></i></span><span style="float: left">Deschiderea coletului la livrare</span>
                    </div>
                    <div class="clearfix">
                        <span style="float: left"><i class="fas fa-undo"></i></span><span style="float: left"> Retur gratuit in 30 de zile</span>
                    </div>
                    <span style="color: grey">
                        Alege capacitate SSD:
                    </span>
                    <div class="clearfix">
                        <div class="button-ssd"> 256 GB</div>
                        <div class="button-ssd"> 512 GB</div>
                    </div>
                </div>
                <div class="product-price">
                    <span></span>
                </div>
            </div>
            <div class="recommended-products clearfix">
                <h2>Produse recomandate</h2>
                <div class="recommended-product">
                    <?php foreach ($recommendedProducts as $key => $recommendedProduct): ?>
                        <?php if ($recommendedProduct['productID'] == $productID): ?>
                            <?php
                                $recommendedID = $recommendedProduct['recommendedID'];?>
                    <a href="http://188.240.210.8/web-05/tavi/emag/produs.php?productId=<?php echo $recommendedProduct['recommendedID'];?>&nume=<?php echo $products[$recommendedID]['name']?>"><?php product($products[$recommendedID]);?></a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php endforeach; ?>
    <?php
        $footer = footer_funct();
        echo $footer;
    ?>

</div>
</body>
</html>