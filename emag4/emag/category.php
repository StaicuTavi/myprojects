<?php
    include "functions/function.php";
    $products = readCSV('CSV/products.csv');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css" integrity="sha384-xxzQGERXS00kBmZW/6qxqJPyxW3UR0BPsL4c8ILaIWXva5kFi7TxkIIaMiKtqV1Q" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/category-style.css">
</head>
<body>
    <div class="container">
        <?php
            $head = header_funct();
            echo $head;
        ?>
        <div class="content-background">
            <div class="content">
                <div class="breadcrumbs">
                    <span class="bc1">Laptop, Tablete & Telefoane</span> /<span class="bc2"> Laptopuri si accesorii</span><span style="font-size: x-small"> / Laptopuri</span></div>
                <div class="content-container clearfix">
                    <div class="sidebar">
                        <div class="sb1">
                            <span class="sbtitle" style="color:#005eb8;">Laptopuri si accesorii</span>
                            <ul>
                                <li><strong>Laptopuri</strong></li>
                                <li>Genti de laptop</li>
                                <li>Docking stations</li>
                                <li>Standuri/Coolere notebook</li>
                                <li>Hard disk-uri notebook</li>
                                <li>Memorii Notebook</li>
                                <li>Incarcatoare laptop</li>
                                <li>Baterii laptop</li>
                                <li>Display Laptop</li>
                                <li>+ mai mult</li>
                            </ul>
                        </div>
                        <div class="sb2">
                            <span class="sbtitle">Disponibilitate</span>
                            <ul>
                               <li>
                                   <input type="checkbox" id="cb1">
                                   <label for="cb1"> In stoc<span class="grey">(1493)</span></label>
                               </li>
                                <li>
                                    <input type="checkbox" id="cb2">
                                    <label for="cb2"> Promotii<span class="grey">(1254)</span></label>
                                </li>
                                <li>
                                    <input type="checkbox" id="cb3">
                                    <label for="cb3"> Resigilate<span class="grey">(433)</span></label>
                                </li>
                                <li>
                                    <input type="checkbox" id="cb4">
                                    <label for="cb4"> Noutati<span class="grey">(332)</span></label>
                                </li>
                                <li>
                                    <input type="checkbox" id="cb5">
                                    <label for="cb5"> Precomanda<span class="grey">(1)</span></label>
                                </li>
                            </ul>
                        </div>
                        <div class="sb3">
                            <span class="sbtitle">eMAG Genius</span>
                            <ul>
                                <input type="checkbox" id="cb6">
                                <label for="cb6"> Da<span class="grey">(827)</span></label>
                            </ul>
                        </div>
                        <div class="sb4"><ul></ul></div>
                        <div class="sb5"><ul></ul></div>
                    </div>
                    <div class="products-container clearfix">
                        <div class="filters">
                            <div>
                                <span style="font-weight: bold;font-size: 25px">Laptopuri&nbsp;&nbsp;&nbsp;</span><span style="font-size: 20px">1703 produse<br/></span>
                                <span style="font-size: 12px; background-color: orangered; color: #e7eff8">Nou</span><span>Prea multe optiuni?</span><span class="">Gaseste modelul potrivit<hr></span>
                            </div>
                            <div class="clearfix">
                                <div class="left">
                                    Brand:<br/>
                                    <form style="width: 100px">
                                        <label></label>
                                        <select style="width: 150px; height: 35px;">
                                            <option value="Selecteaza...">Selecteaza...</option>
                                        </select>
                                    </form>
                                </div>
                                <div class="left">
                                    Tip procesor:<br/>
                                    <form style="width: 100px">
                                        <label></label>
                                        <select style="width: 150px; height: 35px;">
                                            <option value="Selecteaza...">Selecteaza...</option>
                                        </select>
                                    </form>
                                </div>
                                <div class="left">
                                    Tip laptop:<br/>
                                    <form style="width: 100px">
                                        <label></label>
                                        <select style="width: 150px; height: 35px;">
                                            <option value="Selecteaza...">Selecteaza...</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                            <div><hr></div>
                            <div class="clearfix">
                                <div class="left">Ordoneaza dupa:</div>
                                <div class="left">
                                    <form style="">
                                        <label></label>
                                        <select style="width: 150px; height: 25px;">
                                            <option value="Cele mai populare">Cele mai populare</option>
                                        </select>
                                    </form>
                                </div>
                                <div class="left">Produse:</div>
                                <div class="left">
                                    <form style="">
                                        <label></label>
                                        <select style="width: 150px; height: 25px;">
                                            <option value="60 pe pagina">60 pe pagina</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                            <div><hr></div>
                        </div>
                        <div class="products clearfix">
                            <?php
                                $categoryName = $_GET['category'];

                                foreach($products as $key => $product) {
                                    if ($product['category'] == $categoryName) {
                                        ?>
                                        <a href="http://188.240.210.8/web-05/tavi/emag/produs.php?productId=<?php echo $key;?>&nume=<?php echo $product['name']?>"><?php product($product); ?> </a>
                                <?php
                                    }
                                }
                                ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            $footer = footer_funct();
            echo $footer;
        ?>
    </div>
</body>
</html>