<?php
function readCSV($filename){
    $fileLines = file($filename);
    $data = [];
    $header =  str_getcsv($fileLines[0]);
    unset($fileLines[0]);
    foreach ($fileLines as $fileLine){
        $data[] = array_combine($header, str_getcsv($fileLine));
    }
    return $data;
}
function search($items, $key, $value)
{
    $result = [];
    foreach ($items as $item){
        if ($value == $item[$key]){
            $result[]=$item;
        }
    }

    return $result;
}
function product($product){
?>
    <div class="product margin-top">
        <div class="text-center">
            <img class="" src="images/<?php echo $product['image']; ?>" />
        </div>
        <div class="mb-0 pb-0">
            <?php
            $star = (int)($product['stars']);
            ?>
            <p class="text-center mb-0 pb-0">
                <?php
                for($i = 0; $i< $star;$i++){
                    echo "&#11088;";
                }
                ?>
            </p>
            <p class="text-center">66 de review-uri</p>
        </div>
        <div class="mb-0 pb-0">
            <p class="text-center mb-0 pb-0 font-weight-bold">
                <?php echo $product['name']; ?>
            </p>
            <p class="text-center mb-0 pb-0">în stoc</p>
            <p class="mb-0 pb-0 text-center">
                <span class="">Livrat de</span> <?php echo $product['vendor'];?>
            </p>
            <p class="text-center mb-0 pb-0 text-danger"><s><?php echo $product['old_price']; ?> <span>Lei</span></s><span
                    class="">(-41%)
                </span></p>
            <p class="text-center mb-0 pb-0"><?php echo $product['price']; ?> <span>Lei</span></p>
            <p class="text-center"><button type="submit" class="btn btn-primary"><i class=""></i>Adauga in Cos</button></p>
        </div>
    </div>
<?php
}
?>