<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css" integrity="sha384-xxzQGERXS00kBmZW/6qxqJPyxW3UR0BPsL4c8ILaIWXva5kFi7TxkIIaMiKtqV1Q" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css"/>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Home</title>
    <?php
        include 'functions.php';
        $categories = readCSV('CSV/categories.csv');
        $products = readCSV('CSV/products.csv');
    ?>
</head>
<body>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-2"><a href="#"><img src="images/logo.svg" height="36"/></a></div>
                <div class="col-4 search-bar">
                    <form>
                        <input class="corner" type="text" placeholder="Ai libertatea sa alegi ce vrei" name="search">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
                        </svg>
                    </form>
                </div>
                <div class="col-2">
                    <i class="far fa-user"></i>
                    Contul meu
                </div>
                <div class="col-2">
                    <i class="far fa-heart"></i>
                    Favorite
                </div>
                <div class="col-2">
                    <i class="fas fa-shopping-cart"></i>
                    Cosul meu
                </div>
            </div>
        </div>
        <div class="container-fluid nav-bar">
            <div class="container">
                <div class="row mt-3 pb-2 ">
                    <div class="col-1"><a href="#">Produse</a></div>
                    <div class="col-2"><a href="#">Genius | un serviciu premium eMAG</a></div>
                    <div class="col-2"><a href="#">Doneaza pentru linia intai</a></div>
                    <div class="col-1"><a href="#">Resigilate</a></div>
                    <div class="col-2"><a href="#">Necesare zi de zi</a></div>
                    <div class="col-2"><a href="#">Extra-reducerile momentului</a></div>
                    <div class="col-1"><a href="#">Outlet</a></div>
                    <div class=" col-1"><a href="#">
                        <i class="fas fa-headphones"></i>
                        eMAG Help
                    </a></div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 pr-0 pl-0">
                    <img class="banner" src="images/banner.png"/>
                </div>
            </div>
        </div>
    </div>
    <div class="container content">
        <div class="row">
            <div class="col-3 menu">
                <?php foreach ($categories as $category):?>
                    <div class="row mb-2">
                        <div class="col-12 menu-element">
                            <a href="category.page.php?category=<?php echo $category['name']; ?>">
                                <i class="fa <?php echo $category['icon']; ?> menu-icon"></i>
                                <span class=""><?php echo $category['name']; ?></span>
                            </a>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
            <div class="col-9"><img class="content-banner" src="images/banner3.png"/></div>
        </div>
        <div class="row mt-4 widgets">
            <div class="col-5">
                <div class="row">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-4">
                                <img class="widget-benefits-icon" src="https://s12emagst.akamaized.net/layout/ro/images/db//54/80812.png" alt="icon">
                            </div>
                            <div class="col-8 pt-2">
                                <span class="text">Finantare si plata</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-4">
                                <img class="widget-benefits-icon" src="https://s12emagst.akamaized.net/layout/ro/images/db//54/80811.png" alt="icon">
                            </div>
                            <div class="col-8 pt-2">
                                <span class="text">Cea mai variata gama de produse</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-5">
                <div class="row">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-4">
                                <img class="widget-benefits-icon" src="https://s12emagst.akamaized.net/layout/ro/images/db//54/80813.png" alt="icon">
                            </div>
                            <div class="col-8 pt-2">
                                <span class="text">Deschiderea coletului la livrare</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-4">
                                <img class="widget-benefits-icon" src="https://s12emagst.akamaized.net/layout/ro/images/db//54/80814.png" alt="icon">
                            </div>
                            <div class="col-8 pt-2">
                                <span class="text">Retur gratuit in 30 de zile</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <img class="widget-benefits-icon" src="https://s12emagst.akamaized.net/layout/ro/images/db//54/80815.png" alt="icon">
                <span class="text">Suport 24/7</span>
            </div>
        </div>
        <div class="row bg">
            <?php foreach ($products as $key=>$product):?>
                <?php if($key < 4): ?>
                    <div class="col-3 product-homepage"><?php product($product); ?></div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="container-fluid  pt-4 something">
       <div class="container">
            <div class="row">
               <div class="col-3">
                   <h4 class="font-weight-bold">Descarcă aplicația eMAG</h4>
                   <p class="">Lasă-ne numărul tău de telefon și îți vom trimite link-ul de download.
                   </p>
               </div>
               <div class="col-5">
                   <div class="row">
                       <div class="col-9 mr-0 pr-0">
                           <input class="form-control" type="text" placeholder="Default input">
                       </div>
                       <div class="col-3 ml-0 pl-0">
                           <button type="button" class="btn btn-primary">Trimite SMS</button>
                       </div>
                   </div>
               </div>
               <div class="col-2"><img data-src="https://s12emagst.akamaized.net/assets/ro/images/google-play-badge.svg" src="https://s12emagst.akamaized.net/assets/ro/images/google-play-badge.svg" alt="Store badge" class="lozad" data-loaded="true"></div>
               <div class="col-2"><img data-src="https://s12emagst.akamaized.net/assets/ro/images/apple-store-badge.svg" src="https://s12emagst.akamaized.net/assets/ro/images/apple-store-badge.svg" alt="Store badge" class="lozad" data-loaded="true"></div>
           </div>
       </div>
    </div>
    <div class="container-fluid footer-table">
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <h4 class="text-primary">Servicii pentru clienti</h4>
                    <p class="text-secondary">Deschiderea coletului la livrare</p>
                    <p class="text-secondary">30 de zile drept de retur</p>
                    <p class="text-secondary">Garantii si service</p>
                    <p class="text-secondary">Black Friday eMAG</p>
                    <p class="text-secondary">Plata cu cardul in rate fara dobanda</p>
                    <p class="text-secondary">Finantare in rate prin eCREDIT</p>
                </div>
                <div class="col-3">
                    <h4 class="text-primary">Comenzi si livrare</h4>
                    <p class="text-secondary">Contul meu la eMAG</p>
                    <p class="text-secondary">Cum comand online</p>
                    <p class="text-secondary">Livrarea comenzilor</p>
                    <p class="text-secondary">eMAG Corporate</p>
                    <p class="text-secondary">eMAG Marketplace</p>
                    <p class="text-secondary">Modalitati de finantare si plata</p>
                </div>
                <div class="col-3">
                    <h4 class="text-primary">Suport clienti</h4>
                    <p class="text-secondary">Formular reparatie produs</p>
                    <p class="text-secondary">Formular returnare produs</p>
                    <p class="text-secondary">Contact</p>
                    <p class="text-secondary">Conditii generale privind furnizarea serviciilor postale</p>
                    <p class="text-secondary">ANPC</p>
                    <p class="text-secondary">ANPC - SAL</p>
                </div>
                <div class="col-3">
                    <h4 class="text-primary">eMAG.ro</h4>
                    <p class="text-secondary">Vreau sa vand pe eMAG</p>
                    <p class="text-secondary">Termeni si conditii</p>
                    <p class="text-secondary">Prelucrarea datelor cu caracter personal</p>
                    <p class="text-secondary">Politica de utilizare Cookie-uri</p>
                    <p class="text-secondary">Solutionarea Online a litigiilor</p>
                    <p class="text-secondary">Programele Fundatiei eMAG</p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>