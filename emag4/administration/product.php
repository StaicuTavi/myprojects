<?php
    include 'functions.php';
    $products = readCSV('csv/products.csv');
    $pictures = readCSV('csv/pictures.csv');
    $reviews = readCSV('csv/reviews.csv');
    $recomandions = readCSV('csv/recomandations.csv');
    $ratings = readCSV('csv/ratings.csv');
    $lavasoftProducts = search($products,'vendor', 'Lavasoft');
    $appleProducts = search($lavasoftProducts,'name', 'Laptop Apple');
    $progressBarColor = [
        'bg-danger',
        'bg-warning',
        'bg-warning',
        'bg-info',
        'bg-success'
    ];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Product List</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="stylesheet.css">
</head>
<body>
<div class="">
    <div class="alert alert-dark">
        <h1>Product list</h1>
        <h3>You have <?php echo count($products); ?> products </h3>
        <h3>You have <?php echo count($lavasoftProducts); ?> Lavasoft  products </h3>
        <h3>You have <?php echo count($appleProducts); ?> Lavasoft Laptop Apple products </h3>
    </div>
    <table width="100%" border="1" class="table table-hover table-responsive table-striped">
        <tr class="thead-dark">
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>OldPrice</th>
            <th>Vendor</th>
            <th>Images</th>
            <th>Review Nr</th>
            <th>Recom Nr</th>
            <th>Recom to Nr</th>
            <th>Rating by reviews</th>
        </tr>
        <?php foreach ($products as $productId => $product): ?>
            <tr>
                <td>
                    <a target="_blank" href="http://188.240.210.8/web-05/alina/Tema3/eMAG-divs/csv-homework/product.page.php?productIndex=<?php echo $productId ?>">
                        <?php echo $productId ?>
                    </a>
                </td>
                <td><?php echo $product['name'] ?></td>
                <td><?php echo $product['price'] ?> RON</td>
                <td><?php echo $product['oldPrice'] ?> RON</td>
                <td><?php echo $product['vendor'] ?> (<?php echo count(search($products, 'vendor', $product['vendor'])); ?>)</td>
                <td>
                    <?php $searchedImages= search($pictures,'product_id', $productId); ?>
                    <?php foreach ($searchedImages as $picture): ?>
                        <img height="30" src="images/<?php echo $picture['picture']; ?>" />
                    <?php endforeach; ?>
                </td>
                <td width="5px">
                    <?php $searchedReviews= search($reviews,'product_id', $productId); ?>
                    <?php echo count($searchedReviews); ?>
                </td>
                <td width="5px">
                    <?php echo count(search($recomandions,'product_id', $productId));?>
                </td>
                <td width="10px">
                    <?php if (count(search($recomandions,'recomandation_id', $productId)) > 0):?>
                        <?php echo count(search($recomandions,'recomandation_id', $productId));?>
                    <?php else:?>
                    Nu este recomandat
                    <?php endif; ?>
                </td>
                <td width="350px">
                    <?php if (count(search($ratings,'product_id', $productId)) > 0):?>
                        <?php
                            echo 'Review-uri: '.count(search($ratings,'product_id', $productId));
                            $nrOfTotalReviews = count(search($ratings,'product_id', $productId));
                            echo '<hr>';
                            ?>
                            <div class="clearfix">
                                <div class="rating-stars clearfix">
                                    <span class="rating"><strong><?php echo averageRating($ratings,'product_id', $productId);?></strong><br/></span>
                                    <?php
                                        echo starRating($ratings, $productId);
                                    ?>
                                </div>
                                <div class="number-of-stars">
                                    <?php
                                    for($stars = 5; $stars > 0; $stars--) :
                                        echo ($stars) . ' stele:' . count(search(search($ratings, 'product_id', $productId), 'rating', ($stars))) ;
                                        $nrOfEachReviews = count(search(search($ratings, 'product_id', $productId), 'rating', ($stars)));
                                        $percentage = (int)(($nrOfEachReviews/$nrOfTotalReviews)*100);
                                    ?>
                                    <div class="progress">
        <div class="progress-bar <?php echo $progressBarColor[$stars-1]; ?>" role="progressbar" style="width:<?php echo $percentage ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>
<?php endfor;?>
</div>
</div>
<?php else:?>
    Nu are rating
<?php endif; ?>
    </td>
    </tr>
<?php endforeach;?>
    </table>
</div>
</body>
</html>