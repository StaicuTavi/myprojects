<?php


class BundleProduct extends Product
{
    /** @var Product[] */
    public $products;

    public function __construct($id, $products)
    {
        $this->products = $products;
        parent::__construct($id);
    }

    public function getFinalPrice()
    {
        $price = 0;
        foreach ($this->products as $product){
            $price+=$product->getFinalPrice();
        }

        return ceil($price*((100-$this->discount)/100))-0.01;
    }

    public function getName()
    {
        $name = $this->name."(";
        foreach ($this->products as $product){
            $name.=" ".$product->name;
        }
        $name.=')';

        return $name;
    }
}