<?php


class Address extends BaseTable
{
    public $id;
    public $user_id;
    public $address;

    /**
     * User constructor.
     */
    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->user_id = $data['user_id'];
        $this->address = $data['address'];
    }

    public static function getTable()
    {
        return 'addresses';
    }

}