<?php
include "functions.php";
include "class/BaseTable.php";
include "class/Product.php";
include "class/Picture.php";
include "class/EWProduct.php";
include "class/BundleProduct.php";
include "class/Review.php";
include "class/User.php";
include "class/Address.php";
include "class/Favorite.php";
include "class/Category.php";

include "parts/header.php";


?>

<div class="clearfix"></div>
<!-- end section -->

<section class="sec-bpadding-2">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php include "parts/sidebar.php"; ?>
            </div>


            <?php
                $category = Category::find($_GET['id']);
            ?>
            <div class="col-md-9">
                <?php
                foreach ($category->getProducts() as $product){
                    productTile($product);
                }
                ?>
                <!--end item-->

            </div>
        </div>
    </div>
</section>

<?php  include "parts/footer.php"; ?>
