<?php


class product
{
    public $id;

    public $name;

    public $price;

    /** @var  category */
    public $category;


    public function __construct($id, $name, $price, category $category)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->category = $category;
    }

    /**
     * @return mixed
     */

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return product
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price - $this->category->getDiscount() / 100 * $this->price;
    }

    /**
     * @param mixed $price
     * @return product
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param category $category
     * @return product
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }


}