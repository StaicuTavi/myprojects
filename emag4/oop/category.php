<?php

class Category
{
    public $name;

    public $discount;

    /**
     * category constructor.
     * @param $name
     * @param $discount
     */
    public function __construct($name, $discount)
    {
        $this->name = $name;
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        if(date('d') == 10){
            return 2*$this->discount;
        }

        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }


}