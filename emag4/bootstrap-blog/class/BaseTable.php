<?php


abstract class BaseTable
{
    public $id;

    static abstract function getTable();

    public function __construct($data)
    {
        foreach ($data as $column=>$value){
            $this->$column=$value;
        }
    }

    public function delete()
    {
        global $mysqli;
        $table = static::getTable();
        $query = mysqli_query($mysqli, "DELETE FROM `$table` WHERE id=".intval($this->id));
    }

    public function save()
    {
        if ($this->id){
            $this->update();
        } else {
            $this->insert();
        }
    }

    private function update()
    {
        global $mysqli;
        $data = get_object_vars($this);
        $table = static::getTable();
        $sets = [];
        foreach ($data as $column=>$value){
            $sets[]=mysqli_real_escape_string($mysqli, $column)."`='".mysqli_real_escape_string($mysqli, $value);
        }
        $setsList = implode("',`", $sets);

        $query = mysqli_query($mysqli, "UPDATE `$table` SET `$setsList' WHERE id=".intval($this->id).";");
    }

    private function insert()
    {
        global $mysqli;
        $data = get_object_vars($this);
        $table = static::getTable();
        $columns =[];
        $values =[];
        foreach ($data as $column=>$value){
            $columns[]=mysqli_real_escape_string($mysqli, $column);
            $values[]=mysqli_real_escape_string($mysqli, $value);
        }
        $columnsList = implode('`, `', $columns);
        $valuesList = implode("','", $values);

        $query = mysqli_query($mysqli, "INSERT INTO `$table` (`$columnsList`) VALUES ('$valuesList');");

        $this->id = mysqli_insert_id($mysqli);
    }

    /**
     * @param $filters
     * @return static[]
     */
    static function findBy($filters)
    {
        global $mysqli;
        $table = static::getTable();
        $sqlCriterias = [];
        foreach ($filters as $column => $value){
            $sqlCriterias[] = $column.'="'.mysqli_real_escape_string($mysqli, $value).'"';
        }

        $query = mysqli_query($mysqli, 'SELECT * FROM `'.$table.'` WHERE '.implode(' AND ', $sqlCriterias));

        if ($query===false){
            die('SQL error: '. mysqli_error($mysqli));
        }

        $dbData = $query->fetch_all(MYSQLI_ASSOC);

        $result = [];
        foreach ($dbData as $data){
            $class = static::class;
            $result[] = new $class($data);
        }

        return $result;

    }

    /**
     * @param $filters
     * @return false|static
     */
    static function findOneBy( $filters)
    {
        $result = self::findBy($filters);

        if (count($result) > 0){
            return $result[0];
        }

        return false;
    }

    /**
     * @param $id
     * @return false|static
     */
    static function find($id)
    {
        return self::findOneBy(['id'=>$id]);
    }
}