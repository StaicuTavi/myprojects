<?php
session_start();
$mysqli = mysqli_connect("127.0.0.1", "root", "scoalait123", "web-05-tavi");


function readMySQL($table){
    global $mysqli;
    $query = mysqli_query($mysqli, 'SELECT * FROM `'.$table.'`;');

    return $query->fetch_all(MYSQLI_ASSOC);
}

function readCSV($fileName)
{
    $fileLines = file($fileName);
    $data = [];
    $header = str_getcsv($fileLines[0]);
    unset($fileLines[0]);

    foreach ($fileLines as $fileLine) {
        $data[] = array_combine($header, str_getcsv($fileLine));
    }

    return $data;
}

function search($items, $key, $value)
{
    $result = [];
    foreach ($items as $item){
        if ($value == $item[$key]){
            $result[]=$item;
        }
    }

    return $result;
}

function searchMySQL($table, $column, $value)
{
    global $mysqli;
    $query = mysqli_query($mysqli, 'SELECT * FROM `'.$table.'` WHERE '.$column.'="'.$value.'";');

    return $query->fetch_all(MYSQLI_ASSOC);
}

function findBy($table, $filters)
{
    global $mysqli;
    $sqlCriterias = [];
    foreach ($filters as $column => $value){
        $sqlCriterias[] = $column.'="'.mysqli_real_escape_string($mysqli, $value).'"';
    }
    $query = mysqli_query($mysqli, 'SELECT * FROM `'.$table.'` WHERE '.implode(' AND ', $sqlCriterias));

    return $query->fetch_all(MYSQLI_ASSOC);
}

function findOneBy($table, $filters)
{
    $result = findBy($table, $filters);

    if (count($result) > 0){
        return $result[0];
    }

    return false;
}

function isLoggedin(){
    return isset($_SESSION['user_id']);
}