<?php
include 'functions.php';
$categories = readMySQL('category');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add product</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-4"></div>
        <div class="col-4 border rounded-lg border-info">
            <form action="add_products.php" method="post">
                <div class="form-group">
                    <label for="input1">Name:</label>
                    <input type="text" name="name" class="form-control" id="input1" aria-describedby="emailHelp" required>
                </div>
                <div class="form-group">
                    <label for="input2">Price:</label>
                    <input type="text" name="price" class="form-control" id="input2" required>
                </div>
                <div class="form-group">
                    <label for="input3">Old Price:</label>
                    <input type="text" name="oldPrice" class="form-control" id="input3" required>
                </div>
                <div class="form-group">
                    <label for="input4">Vendor:</label>
                    <input type="text" name="vendor" class="form-control" id="input4" required>
                </div>
                <div class="form-group">
                    <label for="input5">Image:</label>
                    <input type="text" name="image" class="form-control" id="input5" required>
                </div>
                <?php foreach ($categories as $category):?>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="category" id="category" value="<?php echo $category['name'] ?>" checked required>
                    <label class="form-check-label" for="category">
                        <?php echo $category['name'] ?>
                    </label>
                </div>
                <?php endforeach; ?>
                <button type="submit" class="btn btn-primary mt-4 mb-2">Submit</button>
            </form>
        </div>
        <div class="col-4"></div>
    </div>
</div>
</body>
</html>