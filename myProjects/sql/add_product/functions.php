<?php
$mysqli = mysqli_connect("188.240.210.8", "root", "scoalait123", "web-05-tavi");


function readMySQL($table){
    global $mysqli;
    $query = mysqli_query($mysqli, 'SELECT * FROM `'.$table.'`;');

    return $query->fetch_all(MYSQLI_ASSOC);
}

function readCSV($fileName)
{
    $fileLines = file($fileName);
    $data = [];
    $header = str_getcsv($fileLines[0]);
    unset($fileLines[0]);

    foreach ($fileLines as $fileLine) {
        $data[] = array_combine($header, str_getcsv($fileLine));
    }

    return $data;
}

function search($items, $key, $value)
{
    $result = [];
    foreach ($items as $item){
        if ($value == $item[$key]){
            $result[]=$item;
        }
    }

    return $result;
}

function searchMySQL($table, $column, $value)
{
    global $mysqli;
    $query = mysqli_query($mysqli, 'SELECT * FROM `'.$table.'` WHERE '.$column.'="'.$value.'";');

    return $query->fetch_all(MYSQLI_ASSOC);
}
function endsWith($string, $endString)
{
    $len = strlen($endString);
    if ($len == 0) {
        return true;
    }
    return (substr($string, -$len) == $endString);
}

