<?php
include "functions.php";

if (!isLoggedin()){
    header('Location: account.php');
    die;
}
?>

<h1>User Profile</h1>

<?php
    $addresses = findBy('user-addresses',['user_id' => $_SESSION['user_id']]);
    echo 'Addresses:<br />';
    foreach ($addresses as $address){
        echo $address['address'].'<br />';
    }
?>

