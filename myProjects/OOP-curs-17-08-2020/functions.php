<?php
session_start();
$mysqli = mysqli_connect("127.0.0.1", "root", "scoalait123", "web-05-daniel");


function readMySQL($table){
    global $mysqli;
    $query = mysqli_query($mysqli, 'SELECT * FROM `'.$table.'`;'); 

    return $query->fetch_all(MYSQLI_ASSOC);
}

function readCSV($fileName)
{
    $fileLines = file($fileName);
    $data = [];
    $header = str_getcsv($fileLines[0]);
    unset($fileLines[0]);

    foreach ($fileLines as $fileLine) {
        $data[] = array_combine($header, str_getcsv($fileLine));
    }

    return $data;
}

function search($items, $key, $value)
{
    $result = [];
    foreach ($items as $item){
        if ($value == $item[$key]){
            $result[]=$item;
        }
    }

    return $result;
}

function searchMySQL($table, $column, $value)
{
    global $mysqli;
    $query = mysqli_query($mysqli, 'SELECT * FROM `'.$table.'` WHERE '.$column.'="'.$value.'";');

    return $query->fetch_all(MYSQLI_ASSOC);
}

function delete($table, $id)
{
    global $mysqli;
    $query = mysqli_query($mysqli, "DELETE FROM `$table` WHERE id=".intval($id));
}

function insert($table, $data)
{
    global $mysqli;
    $columns =[];
    $values =[];
    foreach ($data as $column=>$value){
        $columns[]=mysqli_real_escape_string($mysqli, $column);
        $values[]=mysqli_real_escape_string($mysqli, $value);
    }
    $columnsList = implode('`, `', $columns);
    $valuesList = implode("','", $values);

    $query = mysqli_query($mysqli, "INSERT INTO `$table` (`$columnsList`) VALUES ('$valuesList');");

    return mysqli_insert_id($mysqli);
}

function update($table, $data, $id)
{
    global $mysqli;
    $sets = [];
    foreach ($data as $column=>$value){
        $sets[]=mysqli_real_escape_string($mysqli, $column)."`='".mysqli_real_escape_string($mysqli, $value);
    }
    $setsList = implode("',`", $sets);

    $query = mysqli_query($mysqli, "UPDATE `$table` SET `$setsList' WHERE id=".intval($id).";");
}

function isLoggedin(){
    return isset($_SESSION['user_id']);
}

function productTile(Product $product){
    echo '
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="sp-feature-box-3 margin-bottom">
                <div class="img-box"> <a href="#" class="view-btn uppercase">View</a>
                    <div class="badge">'.$product->vendor.'</div>
                    <img src="img/'.$product->picture.'" alt="" class="img-responsive"/> </div>
                <div class="clearfix"></div>
                <br/>
                <h5 class="less-mar-1">'.$product->name.'</h5>
                <p>'.$product->stoc.'</p>
                <h5 class="text-primary">$ '.$product->getFinalPrice().'</h5>
                <br/>
                <a class="btn btn-border light btn-small" href="#">Add to cart</a> 
            </div>
        </div>
    ';
}