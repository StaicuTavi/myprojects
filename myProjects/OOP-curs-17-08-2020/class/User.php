<?php


class User extends BaseTable
{
    public $id;
    public $email;
    public $password;
    public $status;

    /**
     * User constructor.
     */
    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->email = $data['email'];
        $this->password = $data['password'];
        $this->status = $data['status'];
    }

    public static function getTable()
    {
        return 'users';
    }

    public function getAddresses()
    {
        return Address::findBy(['user_id'=>$this->id]);
    }

    public function getFavorites()
    {
        return Favorite::findBy(['user_id'=>$this->id]);
    }


}