<?php


class Picture extends BaseTable
{
    public $id;
    public $product_id;
    public $image;

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->product_id = $data['product_id'];
        $this->image = $data['image'];
    }

    public static function getTable()
    {
        return 'pictures';
    }

    public function getProduct()
    {
        return Product::find($this->product_id);
    }
}