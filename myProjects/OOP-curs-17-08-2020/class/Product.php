<?php


class Product extends BaseTable
{
    public $id;

    public $name;

    public $picture;

    private $oldPrice;

    public $discount;

    public $stoc;

    public $vendor;


    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->picture = $data['picture'];
        $this->oldPrice = $data['oldPrice'];
        $this->discount = $data['discount'];
        $this->stoc = $data['stoc'];
        $this->vendor = $data['vendor'];
    }

    /**
     * @return mixed
     */
    public function getOldPrice()
    {
        return $this->oldPrice;
    }




    public function getFinalPrice()
    {
        return ceil($this->oldPrice*((100-$this->discount)/100))-0.01;
    }

    /**
     * @return Review[]
     */
    public function getReviews()
    {
        return Review::findBy(['product_id'=>$this->id]);
    }

    /**
     * @return Picture[]
     */
    public function getPictures()
    {
        return Picture::findBy(['product_id'=>$this->id]);
    }

    public static function getTable()
    {
        return 'products';
    }

}


