<?php


class Category extends BaseTable
{
    public $id;

    public $name;

    /**
     * Category constructor.
     * @param $id
     * @param $name
     */
    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->name = $data['name'];
    }

    public static function getTable()
    {
        return 'categories';
    }

    public function getProducts()
    {
        return Product::findBy(['category'=>$this->name]);
    }


}