<?php


class EWProduct extends Product
{
    /** @var Product */
    public $parentProduct;

    public function __construct($id, $parentProduct)
    {
        $this->parentProduct = $parentProduct;
        parent::__construct($id);
        $this->name.= '('.$parentProduct->name.')';
    }

    public function getFinalPrice()
    {
        return ceil(0.1 * $this->parentProduct->getFinalPrice())-0.01;
    }


}