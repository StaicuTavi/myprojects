<?php


class BaseTable
{
    /**
     * @param $filters
     * @return static[]
     */
    static function findBy($filters)
    {
        global $mysqli;
        $table = static::getTable();
        $sqlCriterias = [];
        foreach ($filters as $column => $value){
            $sqlCriterias[] = $column.'="'.mysqli_real_escape_string($mysqli, $value).'"';
        }

        $query = mysqli_query($mysqli, 'SELECT * FROM `'.$table.'` WHERE '.implode(' AND ', $sqlCriterias));

        if ($query===false){
            die('SQL error: '. mysqli_error($mysqli));
        }

        $dbData = $query->fetch_all(MYSQLI_ASSOC);

        $result = [];
        foreach ($dbData as $data){
            $class = static::class;
            $result[] = new $class($data);
        }

        return $result;

    }

    /**
     * @param $filters
     * @return false|static
     */
    static function findOneBy( $filters)
    {
        $result = self::findBy($filters);

        if (count($result) > 0){
            return $result[0];
        }

        return false;
    }

    /**
     * @param $id
     * @return false|static
     */
    static function find($id)
    {
        return self::findOneBy(['id'=>$id]);
    }
}