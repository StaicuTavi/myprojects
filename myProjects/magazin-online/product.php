<?php
include "functions.php";
include "class/BaseTable.php";
include "class/Product.php";
include "class/Image.php";
include "class/User.php";
include "class/Favorite.php";
include "class/Category.php";

$product = Product::find($_GET['id']);
//var_dump($product);die;


include "parts/header.php";

?>
<section class="sec-bpadding-2">
    <div class="container">
        <div class="row">
            <section class="sec-padding">
                <div class="container">


                    <div class="col-md-8 col-sm-12 col-xs-12  margin-bottom">
                        <div class="product_preview_left">
                            <div class="gallery">

                                <div class="full">
                                    <!-- first image is viewable to start -->
                                    <img src="img/<?php echo $product->image; ?>" alt=""> </div>


                                <div class="previews">
                                    <?php
                                        $images = Image::findBy(['product_id'=>$_GET['id']]);
                                        foreach ($images as $image):
                                    ?>
                                        <a data-full="http://via.placeholder.com/850x1000"><img src="img/<?php echo $image->image; ?>" alt=""></a>
                                    <?php endforeach; ?>
                                </div>


                            </div>
                        </div>
                    </div>
                    <!--end left-->

                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <h4 class=" font-weight-7"><?php echo $product->name; ?></h4>
                        <div class="divider-line solid light"></div>
                        <br>
                        <div class="col-md-8"><h2 class="text-primary font-weight-4"><?php echo $product->getFinalPrice(); ?> Lei</h2></div>
                        <div class="col-md-4">
                            <div class="stars">
                                <?php
                                    $stars = $product->stars;
                                    if(isset($stars)):
                                        for ($i = 0; $i < $stars; $i++):
                                ?>
                                        <span><i class="fa fa-star" aria-hidden="true"></i>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <br>
                        <a class="btn btn-dark-3 btn-medium uppercase" href="#">Add to Cart</a>
                        <div class="clearfix"></div>
                        <br><br>
                        <div class="divider-line solid light"></div>
                        <br>
                        <div class="pro-details"><span>Product ID :</span> <?php echo $product->id; ?></div>
                        <div class="pro-details"><span>Category :</span> <?php echo $product->getCategory()->name; ?> </div>
                        <div class="clearfix"></div>
                        <br><br>
                        <ul class="sp-sc-icons">
                            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="active" href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-renren" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>

                    </div>
                    <!--end right-->
                </div>
            </section>
        </div>
    </div>
</section>

<?php  include "parts/footer.php"; ?>