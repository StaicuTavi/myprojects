<?php

/**
 * Created by PhpStorm.
 * User: ionut
 * Date: 8/12/2020
 * Time: 4:51 PM
 */
class Favorite extends BaseTable
{
    public $id;

    public $user_id;

    public $product_id;

    /**
     * Favorite constructor.
     * @param $id
     * @param $user_id
     * @param $product_id
     */
    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->user_id = $data['user_id'];
        $this->product_id = $data['product_id'];
    }

    public static function getTable()
    {
        return 'favorites';
    }
}