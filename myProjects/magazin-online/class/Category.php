<?php

/**
 * Created by PhpStorm.
 * User: ionut
 * Date: 8/11/2020
 * Time: 5:55 PM
 */
class Category extends BaseTable
{
    public $id;

    public $name;

    public $icon;

    public $discount;

    /**
     * Category constructor.
     * @param $id
     * @param $name
     * @param $icon
     */
    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->icon = $data['icon'];
        $this->discount = $data['discount'];
    }

    public function getProducts(){
        return Product::findBy(['category_id'=>$this->id]);
    }

    public static function getTable()
    {
        return 'categories';
    }


}