
<section class="section-dark sec-padding">
    <div class="container ">
        <div class="row"> <br/>
            <div class="col-md-3 col-sm-12 colmargin clearfix margin-bottom">
                <div class="footer-logo"><img src="images/logo/logo.png" alt=""/></div>
                <ul class="fo-address-info border-light">
                    <li>Address: No.28 - 63739 street lorem ipsum City, Country</li>
                    <li><i class="fa fa-home"></i> Phone: + 1 (234) 567 8901</li>
                    <li><i class="fa fa-phone"></i> Phone: + 1 (234) 567 8901</li>
                    <li><i class="fa fa-fax"></i> Fax: + 1 (234) 567 8901</li>
                    <li class="last"><i class="fa fa-envelope"></i> Email: support@yoursite.com </li>
                </ul>
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-12 col-xs-12 bmargin clearfix margin-bottom">
                <div class="item-holder">
                    <h4 class="uppercase less-mar3 font-weight-5 text-white">featured</h4>
                    <div class="footer-title-bottomstrip white"></div>
                    <div class="clearfix"></div>
                    <div class="fo-posts">
                        <div class="image-left"><img src="http://via.placeholder.com/80x80" alt=""/></div>
                        <div class="text-box-right">
                            <h6 class="less-mar3 uppercase nopadding text-white"><a href="#">Winter Coats </a></h6>
                            <p>$25.99</p>
                            <div class="post-info text-light"> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> </div>
                        </div>
                    </div>
                    <div class=" divider-line solid dark opacity-1 "></div>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <div class="fo-posts">
                        <div class="image-left"><img src="http://via.placeholder.com/80x80" alt=""/></div>
                        <div class="text-box-right">
                            <h6 class="less-mar3 uppercase nopadding text-white"><a href="#">Women's Casual</a></h6>
                            <p>$26.99</p>
                            <div class="post-info text-light"> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end item-->

            <div class="col-md-3 col-xs-12 clearfix margin-bottom">
                <h4 class="uppercase less-mar3 font-weight-5 text-white">Quick Links</h4>
                <div class="clearfix"></div>
                <div class="footer-title-bottomstrip white"></div>
                <ul class="footer-quick-links-4 dark-hover">
                    <li><a href="#"><i class="fa fa-angle-right"></i> Placerat bibendum</a></li>
                    <li><a href="#"><i class="fa fa-angle-right"></i> Ullamcorper odio nec turpis</a></li>
                    <li><a href="#"><i class="fa fa-angle-right"></i> Aliquam porttitor vestibulum ipsum</a></li>
                    <li><a href="#"><i class="fa fa-angle-right"></i> Lobortis enim nec nisi</a></li>
                    <li><a href="#"><i class="fa fa-angle-right"></i> Placerat bibendum</a></li>
                    <li><a href="#"><i class="fa fa-angle-right"></i> Lobortis enim nec nisi</a></li>
                </ul>
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-12 col-xs-12 bmargin clearfix margin-bottom">
                <div class="item-holder">
                    <h4 class="uppercase less-mar3 font-weight-5 text-white">on sale products</h4>
                    <div class="footer-title-bottomstrip white"></div>
                    <div class="clearfix"></div>
                    <div class="fo-posts">
                        <div class="image-left"><img src="http://via.placeholder.com/80x80" alt=""/></div>
                        <div class="text-box-right">
                            <h6 class="less-mar3 uppercase nopadding text-white"><a href="#">fashion Bags </a></h6>
                            <p>$25.99</p>
                            <div class="post-info text-light"> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> </div>
                        </div>
                    </div>
                    <div class=" divider-line solid dark opacity-1 "></div>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <div class="fo-posts">
                        <div class="image-left"><img src="http://via.placeholder.com/80x80" alt=""/></div>
                        <div class="text-box-right">
                            <h6 class="less-mar3 uppercase nopadding text-white"><a href="#">Leather Shoe</a></h6>
                            <p>$26.99</p>
                            <div class="post-info text-light"> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end item-->

        </div>
    </div>
</section>
<div class="clearfix"></div>
<!--end section-->

<section class="sec-padding-6">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 bmargin">
                <ul class="sp-payment-logo-list">
                    <li>
                        <h6>We Accept:</h6>
                    </li>
                    <li><img src="http://via.placeholder.com/50x40" alt=""/></li>
                    <li><img src="http://via.placeholder.com/50x40" alt=""/></li>
                    <li><img src="http://via.placeholder.com/50x40" alt=""/></li>
                    <li><img src="http://via.placeholder.com/50x40" alt=""/></li>
                    <li><img src="http://via.placeholder.com/50x40" alt=""/></li>
                </ul>
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-12 col-xs-12 bmargin">
                <p>Copyright © 2018 l Codelayers.</p>
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-12 col-xs-12 bottom-margin">
                <ul class="sp-sc-icons">
                    <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="active" href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-renren" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
            <!--end item-->
        </div>
    </div>
</section>
<div class="clearfix"></div>
<!-- end section -->

<a href="#" class="scrollup"></a><!-- end scroll to top of the page-->

</div>
<!--end site wrapper-->
</div>
<!--end wrapper boxed-->

<!-- Scripts -->
<script src="js/jquery/jquery.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>

<script src="js/less/less.min.js" data-env="development"></script>

<!-- Scripts END -->

<!-- Template scripts -->
<script src="js/megamenu/js/main.js"></script>
<script src="js/owl-carousel/owl.carousel.js"></script>
<script src="js/owl-carousel/custom.js"></script>
<script src="js/owl-carousel/owl.carousel.js"></script>
<script src="js/owl-carousel/custom.js"></script>
<script type="text/javascript" src="js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="js/cubeportfolio/main-mosaic3.js"></script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS
(Load Extensions only on Local File Systems !
The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript">
    var tpj=jQuery;
    var revapi4;
    tpj(document).ready(function() {
        if(tpj("#rev_slider").revolution == undefined){
            revslider_showDoubleJqueryError("#rev_slider");
        }else{
            revapi4 = tpj("#rev_slider").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/revolution-slider/js/",
                sliderLayout:"auto",
                dottedOverlay:"none",
                delay:9000,
                navigation: {
                    keyboardNavigation:"off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation:"off",
                    onHoverStop:"off",
                    arrows: {
                        style:"erinyen",
                        enable:true,
                        hide_onmobile:true,
                        hide_under:778,
                        hide_onleave:true,
                        hide_delay:200,
                        hide_delay_mobile:1200,
                        tmp:'',
                        left: {
                            h_align:"left",
                            v_align:"center",
                            h_offset:80,
                            v_offset:0
                        },
                        right: {
                            h_align:"right",
                            v_align:"center",
                            h_offset:80,
                            v_offset:0
                        }
                    }
                    ,
                    touch:{
                        touchenabled:"on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    }
                    ,



                },
                viewPort: {
                    enable:true,
                    outof:"pause",
                    visible_area:"80%"
                },

                responsiveLevels:[1240,1024,778,480],
                gridwidth:[1240,1024,778,480],
                gridheight:[650,640,1300,820],
                lazyType:"smart",
                parallax: {
                    type:"mouse",
                    origo:"slidercenter",
                    speed:2000,
                    levels:[2,3,4,5,6,7,12,16,10,50],
                },
                shadow:0,
                spinner:"off",
                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,
                shuffle:"off",
                autoHeight:"off",
                hideThumbsOnMobile:"off",
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                disableProgressBar:"on",
                debugMode:false,
                fallbacks: {
                    simplifyAll:"off",
                    nextSlideOnWindowFocus:"off",
                    disableFocusListener:false,
                }
            });
        }
    });	/*ready*/
</script>


<script>
    $(window).load(function(){
        setTimeout(function(){

            $('.loader-live').fadeOut();
        },1000);
    })

</script>
<script src="js/functions/functions.js"></script>
</body>
</html>
