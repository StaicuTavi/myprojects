<?php


class cartItem
{
    /** @var  product */
    public $product;


    public $quantity;

    /**
     * cartItem constructor.
     * @param product $product
     * @param $quantity
     */
    public function __construct(product $product, $quantity)
    {
        $this->product = $product;
        $this->quantity = $quantity;
    }
    public function calculatePrice(){
        return $this->product->getPrice() * $this->quantity;
    }






}