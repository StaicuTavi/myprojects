<?php


class cart
{
    /** @var array  */
    public $cartItems = [];

    public $priceOfCart;

    public function add($product,$quantity){

        if(count($this->cartItems)==0){
            $this->cartItems[0] = new CartItem($product,$quantity);
        }
        for($i=0; $i<count($this->cartItems);$i++){
            if($this->cartItems[$i]->product == $product ){
                $this->cartItems[$i] = new CartItem($product,$quantity+$this->cartItems[$i]->quantity);
                //$this->cartItems[$i]->quantity += $quantity;
            } else{
                $this->cartItems[$i+1] = new CartItem($product,$quantity);
            }
        }
    }

    public function clearAll()
    {
        foreach ($this->cartItems as $i => $value){
            unset($this->cartItems[$i]);
        }
    }

}