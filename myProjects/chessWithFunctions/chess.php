<html>
<head>
    <title>Chess game</title>
</head>
<body>
<?php
include 'functions.php';
$cellColor = 'lightgrey';
$currentCell = 0;
$chessPieces = [
    '&#9814;', //tura
    '&#9816;', //cal
    '&#9815;', //nebun
    '&#9813;', //regina
    '&#9812;', //rege
    '&#9815;', //nebun
    '&#9816;', //cal
    '&#9814;', //tura
];

//$chessPieces[0][1];
?>
<table border="1" width="800">
    <?php for ($row=0; $row<8; $row++): ?>
        <tr>
            <?php
                checkPieceColor($row);
            ?>
            <?php for ($col=0; $col<8; $col++): ?>
                <?php
                $currentCell++;
                checkCellColor($currentCell);
                addPieceInBoard($row, $col, $chessPieces);
                ?>
                <td width="100" height="100" style="font-size: 70px; text-align: center; color: <?php echo $pieceColor; ?>;" bgcolor="<?php echo $cellColor; ?>">
                    <?php echo $chessPiece; ?>
                </td>
            <?php endfor; ?>
        </tr>
        <?php $currentCell++; ?>
    <?php endfor; ?>
</table>
</body>
</html>