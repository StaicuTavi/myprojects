<?php


function readCSV($fileName)
{
    $fileLines = file($fileName);
    $data = [];
    $header = str_getcsv($fileLines[0]);
    unset($fileLines[0]);

    foreach ($fileLines as $fileLine) {
        $data[] = array_combine($header, str_getcsv($fileLine));
        //var_dump($header);
        //var_dump(str_getcsv($fileLine));

//        $data[] = str_getcsv($fileLine);
    }
    return $data;
}

function get_product($product, $index)
{
    $csvName = $_GET['category'];
    ?>
    <div class="padding-5">
        <div class="product">
            <div>
                <a href="product.page.php?productIndex=<?php echo $index; ?>"
                   target="_blank">
                    <div class="img-prod">
                        <img src="img/category/<?php echo $product['pictures']; ?>"/>
                    </div>
                    <div class="recom-product-rev">
                        <?php echo $product['review']; ?>
                    </div>
                </a>
            </div>
            <div>
                <div class="recom-product-title">
                    <a href="product.page.php?productIndex=<?php echo $index; ?>"
                       target="_blank">
                        <?php echo $product['name']; ?>
                    </a>
                </div>
            </div>
            <div>
                <div><?php echo $product['stoc']; ?></div>
                <div>genius</div>
                <div><span class="old-price"><?php echo $product['oldPrice']; ?>
                        lei</span><span> (-<?php echo $product['discount'] ?>%)</span></div>
                <div class="price"><?php echo $product['price']; ?> lei</div>
                <div class="cart-btn">
                    <div class="add-to-cart-btn">
                        <button><i class="fa fa-shopping-cart"></i> &nbsp;&nbsp; Adauga in Cos</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}


?>


<?php
function get_presentation_product_template($products, $csvIndex)
{
    ?>

    <!--    <div>-->
    <!--        <!--                Product Name or headline -->-->
    <!--        <h1>--><?php //echo $products[$csvIndex]['name'];
    ?><!--</h1>-->
    <!--        <!--                Product cod -->-->
    <!--        <div class="clearfix">-->
    <!--            <span><small>--><?php //echo $products[$csvIndex]['code'];
    ?><!--</small></span>-->
    <!--            <span class="fb-btn-and-comp">-->
    <!--                    <input type="button" value="f Share" class="fb-button"/>-->
    <!--                    <input type="checkbox" name="Compara" value="Compara"> Compara-->
    <!--                </span>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--    <div class="clearFix">-->
    <!--        <div class="product-picture">-->
    <!--            <div>-->
    <!--                <img src="img/$products[$csvIndex]['pictures']" />-->
    <!--            </div>-->
    <!--            <div>-->
    <!--                <div class="inline-block picture">-->
    <!--                    <img src="img/laptop.jpg" />-->
    <!--                </div>-->
    <!--                <div class="inline-block picture">-->
    <!--                    <img src="img/laptop2.jpg" />-->
    <!--                </div>-->
    <!--                <div class="inline-block picture">-->
    <!--                    <img src="img/laptop3.jpg" />-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--        <div class="product-info clearFix">-->
    <!--            <div class="col-1">-->
    <!--                <div>-->
    <!--                    <small>Opinia clientilor</small>-->
    <!--                    <br>-->
    <!--                    <a><strong class="stars">--><?php //echo $products[$csvIndex]['review'];
    ?><!--</strong> 4,95</a> <br>-->
    <!--                    151 review-uri | 10 intrebari <br>-->
    <!--                    Vândut și livrat de: <u>--><?php //echo $products[$csvIndex]['vendor'];
    ?><!--</u> <br>-->
    <!--                    Ajunge in: <strong>Dolj, Craiova</strong> <u>schimba</u> <br/>-->
    <!--                    <br/>-->
    <!--                    <hr/>-->
    <!--                    Livrare standard<br/>-->
    <!--                    Miercuri, 20 Mai – Joi, 21 Mai<br/>-->
    <!--                    <hr/>-->
    <!--                    Beneficii:<br/>-->
    <!--                    <i class="fa fa-gift"></i><strong> 24 puncte</strong> prin cardul eMAG-Raiffeisen <u>detalii</u><br/>-->
    <!--                    14 zile drept de retur <br/>-->
    <!--                    <i class="fa fa-boxes"></i> Deschiderea coletului la livrare<br/>-->
    <!--                    <i class="fa fa-university"></i> Garantie inclusa: <u>detalii</u> <br/>-->
    <!--                    <ul>-->
    <!--                        <li>Persoane fizice: 12 luni <u>extinde</u></li>-->
    <!--                    </ul>-->
    <!--                    Retur gratuit in 30 de zile detalii<br/>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--            <div class="col-2">-->
    <!--                <p>-->
    <!--                    <s>--><?php //echo $products[$csvIndex]['oldPrice'];
    ?><!--<sup>99</sup> Lei</s> (- --><?php //echo $products[$csvIndex]['discount'];
    ?><!--% )<br/>-->
    <!--                    <strong class="price">--><?php //echo $products[$csvIndex]['price'];
    ?><!--<sup>99</sup> Lei</strong> <br>-->
    <!--                    în stoc <br/>-->
    <!--                    Grabeste-te! Inca 12 persoane se uita acum la acest produs<br/>-->
    <!--                    <br/>-->
    <!--                </p>-->
    <!--                <div class="padding-t-b-5">-->
    <!--                    <button class="cart"><i style="float: left; font-size: x-large;" class="fa fa-shopping-cart"></i>-->
    <!--                        &nbsp;&nbsp;&nbsp;Adauga in cos-->
    <!--                    </button>-->
    <!--                </div>-->
    <!--                <div class="padding-t-b-5">-->
    <!--                    <button class="fav"><i style="float: left; font-size: x-large;" class="fas fa-cart-arrow-down"></i>-->
    <!--                        &nbsp;&nbsp;&nbsp;Cumpara in rate-->
    <!--                    </button>-->
    <!--                </div>-->
    <!--                <div class="padding-t-b-5">-->
    <!--                    <button class="fav"><i style="float: left; font-size: x-large;" class="fa fa-heart"></i>-->
    <!--                        &nbsp;&nbsp;&nbsp;Adauga la Favorite-->
    <!--                    </button>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <?php
}

?>
