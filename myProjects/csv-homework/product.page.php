<?php

include "functions.php";

$csvIndex = $_GET['productIndex'];

$products = readCSV('docs-csv/products.csv');
$title = $products[$csvIndex]['category'].'-'.$products[$csvIndex]['name'];

$product = $products[$csvIndex];

//print_r($product);
//
//print_r($products[$csvIndex]['name']);
//
////die();
//get_presentation_product_template($product, $csvIndex);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" href="../css.css">
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;562;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css">
</head>
<body>
<div class="page">
    <!--        Header -->
    <?php include "../parts/2.header.php"; ?>
    <div class="body">
        <!--        Body-->
        <?php include "../parts/product-body.php"; ?>
    </div>
    <!--    Footer-->
    <?php include "../parts/footer.php"; ?>
</div>
</body>
</html>