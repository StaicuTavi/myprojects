<?php
include "functions.php";
$title = $_GET['category'];
$csvName = $_GET['category'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title; ?></title>
    <?php include "../parts/1.category-head.php" ?>
</head>
<body>
<div class="page">
    <?php include "../parts/2.header.php" ?>
    <div class="body gray-bg">
        <div>
            <img class="banner" src="img/banner.png" />
        </div>
        <div class="content">
            <div class="container">
                <div>
                    <!--                Breadcrumb -->
                    <div class="breadcrumb">
                        <ul>
                            <li>Laptop, Tablete & Telefoane</li>
<!--                            <li>Laptopuri si accesorii</li>-->
                            <li>
                                <a href="http://188.240.210.8/web-05/alina/Tema3/eMAG-divs/csv-homework/product.page.php?category=<?php echo $csvName ?>">
                                    <?php echo $title; ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="clearFix">
                    <div class="sidebar">
                        <div class="sidebar-card">
                            <h4>Disponibilitate</h4>
                            <input type="checkbox" title="stoc" />In stoc <br>
                            <input type="checkbox" title="promotii" />Promotii <br>
                            <input type="checkbox" title="noutati" />Noutati <br>
                            <input type="checkbox" title="resigilate" />Resigilate <br>
                            <input type="checkbox" title="precomanda" />Precomanda <br>
                        </div>
                        <div class="sidebar-card">
                            <h4>Pret</h4>
                            <input type="checkbox" title="1" />1000-1500 <br>
                            <input type="checkbox" title="1" />1500-2000 <br>
                            <input type="checkbox" title="1" />2000-3000 <br>
                            <input type="checkbox" title="1" />3000-4000 <br>
                            <input type="checkbox" title="1" />4000-5000 <br>
                            <input type="checkbox" title="1" />Peste 5000 <br>
                            <hr />
                            <input type="checkbox" title="price">Interval pret <br>
                        </div>
                        <div class="sidebar-card">
                            <h4>Rating minim</h4>
                            <input type="checkbox" title="star" />&#x2605;&#x2605;&#x2605;&#x2605;&#x2605;<br>
                            <input type="checkbox" title="star" />&#x2605;&#x2605;&#x2605;&#x2605;&#x2606;<br>
                            <input type="checkbox" title="star" />&#x2605;&#x2605;&#x2605;&#x2606;&#x2606;<br>
                            <input type="checkbox" title="star" />&#x2605;&#x2605;&#x2606;&#x2606;&#x2606;<br>
                            <input type="checkbox" title="star" />&#x2605;&#x2606;&#x2606;&#x2606;&#x2606;<br>
                        </div>
                        <div class="sidebar-card">
                            <h4>Livrare prin Tazz by eMag</h4>
                            <label>Cauta...</label>
                            <input type="search" title="delivery"> <br>
                            <input type="checkbox" title="delivery" />Bucuresti - Sector 1 <br>
                            <input type="checkbox" title="delivery" />Bucuresti - Sector 2 <br>
                            <input type="checkbox" title="delivery" />Bucuresti - Sector 3 <br>
                            <input type="checkbox" title="delivery" />Bucuresti - Sector 4 <br>
                            <input type="checkbox" title="delivery" />Bucuresti - Sector 5 <br>
                            <input type="checkbox" title="delivery" />Bucuresti - Sector 6 <br>
                        </div>
                        <div class="sidebar-card">
                            <h4>Disponibil prin easybox</h4>
                            <input type="checkbox" />Da <br>
                        </div>
                    </div>
                    <div class="category-display">
                        <div class="category-filters">Fliltre<br></div>
                        <div class="grid">
                            <?php include "../parts/category-card.php"; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="gray-bg">
            <?php include "../parts/navigation-history.php"; ?>
        </div>
        <div class="gray-bg">
            <!--            Marketplace-->
            <?php include "../parts/marketplace.php"; ?>
        </div>
    </div>
    <div>
        <!--            Ante-footer - app stores buttons-->
    <?php include "../parts/ante-footer.php"; ?>
    </div>
        <!--    Footer-->
    <?php include "../parts/footer.php" ?>
</div>
</body>
</html>