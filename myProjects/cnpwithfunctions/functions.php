<?php
function Calculations($cnp){
    global $birthMonth, $birthDay, $countyNr, $year, $month, $day, $sex, $age, $birthYear;
    $sex = (int)($cnp/pow(10,12));
    if($sex == 1 || $sex == 2) {
        $birthYear = 1900 + (int)($cnp / pow(10, 10)) % pow(10, 2);
    }else{
        $birthYear = 2000 + (int)($cnp / pow(10, 10)) % pow(10, 2);
    }
    $birthMonth = (int)($cnp/pow(10,8))%pow(10,2);
    $birthDay = (int)($cnp/pow(10,6))%pow(10,2);
    $countyNr = (int)($cnp/pow(10,4))%pow(10,2);
    $year = (int)(date('Y'));
    $month = (int)(date('m'));
    $day = (int)(date('d'));
    if($month < $birthMonth){
        $age = $year - $birthYear - 1;
    }elseif ($month == $birthMonth){
        if($day < $birthDay){
            $age = $year - $birthYear - 1;
        }else{
            $age = $year - $birthYear;
        }
    }else {
        $age = $year - $birthYear;
    }
    if($birthDay > 31){
        $birthDay = 'Format CNP invalid, numarul corespunzator zilei de nastere nu este corect';
    }
}
function verifySex($sex){
    global $gen;
    if($sex == 1 || $sex == 5){
        $gen = 'Masculin';
    }elseif($sex == 2 || $sex == 6){
        $gen = 'Feminin';
    }else{
        $gen = 'Format CNP invalid, indicativul de sex nu are o valoare cunoscuta.';
        die('Format CNP invalid, indicativul de sex nu are o valoare cunoscuta.');
    }
}
function verifyMonth(){
    global $monthArray;
    $monthArray = [
        1 => 'Ianuarie',
        2 => 'Februarie',
        3 => 'Martie',
        4 => 'Aprilie',
        5 => 'Mai',
        6 => 'Iunie',
        7 => 'Iulie',
        8 => 'August',
        9 => 'Septembrie',
        10 => 'Octombrie',
        11 => 'Noiembrie',
        12 => 'Decembrie',
    ];
}
function verifyCounty(){
    global $countyArray;
    $countyArray = [
        1 => 'Alba',
        2 => 'Arad',
        3 => 'Arges',
        4 => 'Bacau',
        5 => 'Bihor',
        6 => 'Bistrita-Nasaud',
        7 => 'Botosani',
        8 => 'Brasov',
        9 => 'Braila',
        10 => 'Buzau',
        11 => 'Caras-Severin',
        12 => 'Cluj',
        13 => 'Constanta',
        14 => 'Covasna',
        15 => 'Dambovita',
        16 => 'Dolj',
        17 => 'Galati',
        18 => 'Gorj',
        19 => 'Harghita',
        20 => 'Hunedoara',
        21 => 'Ialomita',
        22 => 'Iasi',
        23 => 'Ilfov',
        24 => 'Maramures',
        25 => 'Mehedinti',
        26 => 'Mures',
        27 => 'Neamt',
        28 => 'Olt',
        29 => 'Prahova',
        30 => 'Satu Mare',
        31 => 'Salaj',
        32 => 'Sibiu',
        33 => 'Suceava',
        34 => 'Teleorman',
        35 => 'Timis',
        36 => 'Tulcea',
        37 => 'Vaslui',
        38 => 'Valcea',
        39 => 'Vrancea',
        40 => 'Bucuresti',
        41 => 'Bucuresti-Sector 1',
        42 => 'Bucuresti-Sector 2',
        43 => 'Bucuresti-Sector 3',
        44 => 'Bucuresti-Sector 4',
        45 => 'Bucuresti-Sector 5',
        46 => 'Bucuresti-Sector 6',
        51 => 'Calarasi',
        52 => 'Giurgiu',
    ];
}

?>