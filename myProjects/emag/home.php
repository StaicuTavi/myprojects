<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="styles/category-style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css" integrity="sha384-xxzQGERXS00kBmZW/6qxqJPyxW3UR0BPsL4c8ILaIWXva5kFi7TxkIIaMiKtqV1Q" crossorigin="anonymous">
</head>
<body>
<?php
    include 'functions/function.php';
    $categories = readCSV('CSV/categories.csv');
?>
<div class="container">
    <?php
        $head = header_funct();
        echo $head;
    ?>
    <div class="home-content">
        <div class="sidebar-and-banner clearfix">
            <div class="home-sidebar clearfix">
                    <?php foreach($categories as $key => $category): ?>
                        <div class="category-icon-and-name">
                            <a href="http://188.240.210.8/web-05/tavi/emag/category.php?index=<?php echo $key ?>&category=<?php echo $category['name']; ?>"><i class=<?php echo $category['icon'];?>></i>  <?php echo $category['name'];?></a>
                        </div>
                    <?php endforeach;?>
            </div>
            <div class="home-content-banner">
                <img src="../images/banner.PNG"/>
            </div>
        </div>
        <div class="home-content-images">
            <img src="../images/finantaresiplata.PNG">
            <img src="../images/ceamaivariatagamadeproduse.PNG">
            <img src="../images/deschiderea.PNG">
            <img src="../images/retur.PNG">
            <img src="../images/suport.PNG">
        </div>
        <div class="content-gradient-recommendations">
                                <a href="telefon.php?titlu=Telefon" style="text-decoration:none"><p align="center"><img src="../images/telefon.PNG"></p>
                                    <p align="center"><strong>Telefon mobil Samsung</strong></p>
                                    <p align="center"><span>⭐⭐⭐⭐⭐<span style="color:cornflowerblue;">4.3(201)</span></span></p>
                                    <p align="center"><strike>1.199<sup>99</sup></strike>Lei (-30%)<br></p>
                                    <p align="center" style="color: red">839<sup>99</sup>Lei</p></a>

        </div>
    </div>
<?php
    $footer = footer_funct();
    echo $footer;
?>
</div>