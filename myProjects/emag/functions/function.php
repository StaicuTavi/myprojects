<?php
function readCSV($filename){
    $fileLines = file($filename);
    $data = [];
    $header =  str_getcsv($fileLines[0]);
    unset($fileLines[0]);
    foreach ($fileLines as $fileLine){
        $data[] = array_combine($header, str_getcsv($fileLine));
    }
    return $data;
}

function product($product)
{
    ?>
    <div class="product">
        <div class="thumbnail">
            <img class="lozad" src="images/<?php echo $product['image']; ?>" />
        </div>
        <div class="star-rating-text ">
            <?php
                $star = (int)($product['stars']);
            ?>
            <span>
                <?php
                    for($i = 0; $i< $star;$i++){
                        echo "&#11088;";
                    }
                ?>
            </span>
            <span class="hidden-xs ">66 de review-uri</span>
        </div>

        <div class="card-section-mid">
            <h2 class="card-body product-title-zone">
                <?php echo $product['name']; ?>
            </h2>
        </div>
        <div class="bottom-info">
            <div class="card-section-btm">
                <div class="card-body">
                    <p class="product-stock-status text-availability-in_stock">în stoc</p>
                    <p class="product-vendor text-truncate ">
                        <span class="product-sold-by">Livrat de</span> <?php echo $product['vendor'];?>
                    </p>
                </div>
                <div class="card-body">
                    <div class="pricing-old_preserve-space">
                        <p class="product-old-price"><s><?php echo $product['old_price']; ?> <span>Lei</span></s><span
                                class="product-this-deal">(-41%)
                        </span></p>
                        <p class="product-new-price"><?php echo $product['price']; ?> <span>Lei</span></p>
                    </div>
                </div>
                <div class="card-footer hidden-xs">
                    <button type="submit"><i class="em em-cart_fill"></i>Adauga in Cos</button>
                </div>
            </div>
        </div>
    </div>
<?php
}
function header_funct()
{
    ?>
        <div class="header">
            <div class="header-logo clearfix">
                <div class="logo">
                    <a href="#"><img src="../images/logo.svg" height="36"/></a>
                </div>
                <div class="search-bar">
                    <form>
                        <input class="corner" type="text" placeholder="Ai libertatea sa alegi ce vrei" name="search">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <div class="buttons">
                    <ul>
                        <li>
                            <i class="far fa-user"></i>
                            Contul meu
                        </li>
                        <li>
                            <i class="far fa-heart"></i>
                            Favorite
                        </li>
                        <li>
                            <i class="fas fa-shopping-cart"></i>
                            Cosul meu
                        </li>
                    </ul>
                </div>
            </div>
            <div class="header-menu clearfix">
                <ul>
                    <li><a href="#">Produse</a></li>
                    <li><a href="#">Genius | un serviciu premium eMAG</a></li>
                    <li><a href="#">Doneaza pentru linia intai</a></li>
                    <li><a href="#">Resigilate</a></li>
                    <li><a href="#">Necesare zi de zi</a></li>
                    <li><a href="#">Extra-reducerile momentului</a></li>
                    <li><a href="#">Outlet</a></li>
                    <li class="emaghelp"><a href="#">
                            <i class="fas fa-headphones"></i>
                            eMAG Help
                        </a></li>
                </ul>
            </div>
            <div class="header-banner clearfix"><img src="../images/banner.png"/></div>
        </div>
    <?php
}
function footer_funct()
{
    ?>
    <div class="footer">
        <div class="footer-banner">
            <img src="../images/bannerfooter.PNG"/>
        </div>
        <div class="footer-content clearfix">
            <div class="emag-app">
                <h3>Descarca aplicatia eMAG</h3>
                <p>Lasă-ne numărul tău de telefon și îți vom trimite link-ul de download.</p>
            </div>
            <div class="phone-number">
                <form>
                    <input class="inp" type="text" placeholder="07xxxxxxxx">
                    <button class="button button2">Trimite SMS</button>
                </form>
            </div>
            <div class="footer-content-icons">
                <img src="../images/googleplay.PNG"/>
                <img src="../images/appstore.PNG"/>
            </div>
        </div>
        <div class="footer-content-table clearfix">
            <div class="footer-content-col">
                <h3 style="color: #008CBA;">Servicii pentru clienti</h3>
                <p></p>
                <p class="par">Deshiderea coletului la livrare</p>
                <p class="par">30 de zile drept de retur</p>
                <p class="par">Garantii si service</p>
                <p class="par">Black Friday eMAG</p>
                <p class="par">Plata cu cardul in rate fara dobanda</p>
                <p class="par">Finantare in rate prin eCREDIT</p>
                <p><img src="../../images/mastervisacard.PNG"/></p>
            </div>
            <div class="footer-content-col">
                <h3 style="color: #008CBA;">Comenzi si livrare</h3>
                <p></p>
                <p class="par">Contul meu la eMAG</p>
                <p class="par">Cum comand online</p>
                <p class="par">Livrarea comenzilor</p>
                <p class="par">eMAG Corporate</p>
                <p class="par">eMAG Marketplace</p>
                <p class="par">Modalitati de finantare si plata</p>
            </div>
            <div class="footer-content-col">
                <h3 style="color: #008CBA;">Suport clienti</h3>
                <p></p>
                <p class="par">Formular reparatie produs</p>
                <p class="par">Formular returnare produs</p>
                <p class="par">Contact</p>
                <p class="par">Conditii generale privind furnizarea serviciilor<br>postale</p>
                <p class="par">ANPC</p>
                <p class="par">ANPC - SAL</p>
            </div>
            <div class="footer-content-col">
                <h3 style="color: #008CBA;">eMAG.ro</h3>
                <p></p>
                <p class="par">Vreau sa vand pe eMAG</p>
                <p class="par">Termeni si conditii</p>
                <p class="par">Prelucrarea datelor cu caracter personal</p>
                <p class="par">Politica de utilizare Cookie-uri</p>
                <p class="par">Solutionarea Online a litigiilor</p>
                <p class="par">Programele Fundatiei eMAG</p>
            </div>
        </div>
    </div>
<?php
}
?>
