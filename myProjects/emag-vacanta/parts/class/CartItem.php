<?php

/**
 * Created by PhpStorm.
 * User: ionut
 * Date: 8/23/2020
 * Time: 2:30 AM
 */
class CartItem extends BaseTable
{
    public $product_id;

    public $cart_id;

    public $quantity;

    public static function getTable()
    {
        return 'cart-items';
    }

    /** @var Product */
    public function getProduct(){
        return Product::find($this->product_id);
    }

    public function getPrice(){
        return ceil($this->getProduct()->getFinalPrice() * $this->quantity)-0.01;
    }

}