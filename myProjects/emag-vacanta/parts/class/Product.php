<?php

/**
 * Created by PhpStorm.
 * User: ionut
 * Date: 8/11/2020
 * Time: 5:55 PM
 */
class Product extends BaseTable
{
    public $category_id;

    public $name;

    public $image;

    public $old_price;

    public $vendor;

    public $stars;

    public $cod;

    public $breadcrumbs;

    public $discount;


    public function getFinalPrice()
    {
        return ceil($this->old_price*((100-$this->discount)/100))-0.01;
    }

    /** @return Category */
    public function getCategory()
    {
        return Category::find($this->category_id);
    }

    /** @return Image[] */
    public function getImages()
    {
        return Image::findBy(['product_id'=>$this->id]);;
    }

    public static function getTable()
    {
        return 'products';
    }


}