<?php
include "../functions.php";

if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    die($_POST['email']." is invalid");
}

if ($_POST['password'] != $_POST['retype_password']){
    die('Parolele nu coincid');
}


$user = User::findOneBy(['email'=>$_POST['email']]);
if(!$user){
    $user = new User(['email'=>$_POST['email'], 'password'=>md5($_POST['password']), 'avatar'=>$_POST['avatar']]);
    $user->save();
    echo "Userul a fost creat cu succes!!!";
}else{
    echo "Userul exista deja!!!";
}

header('Refresh: 2; url:login.php');
