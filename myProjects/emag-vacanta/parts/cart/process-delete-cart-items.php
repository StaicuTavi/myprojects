<?php
include "../functions.php";

if (!isLoggedin()){
    header("Location: login.php");
    die;
}

$user = getUser();
$cart = $user->getCart();
$cartItem = CartItem::findOneBy(['cart_id'=>$cart->id, 'product_id' => $_GET['product_id']]);

$cartItem->delete();

header("Location:../../Cart.php");
?>
