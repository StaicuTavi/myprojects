<?php
include 'parts/functions.php';

/*$products = findBy('products',['category_id'=>$_GET['category_id']]);
foreach ($products as $product){
    $productObjects[] = new Product($product['id']);
}*/
/** @var Category $category */
$category = Category::find($_GET['category_id']);

/** @var Product[] $products */
$products = $category->getProducts();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script>
        function setFavoritePage()
        {
            var hrefHeader = document.getElementById('mySecondHref');
            hrefHeader.href = 'parts/account/login.php';
            var href = document.getElementById('myHref');
            href.href = 'parts/account/login.php';
        }
    </script>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css" integrity="sha384-xxzQGERXS00kBmZW/6qxqJPyxW3UR0BPsL4c8ILaIWXva5kFi7TxkIIaMiKtqV1Q" crossorigin="anonymous">
    <link rel="stylesheet" href="parts/style.css"/>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title><?php echo $category->name; ?></title>
</head>
<body>
    <?php include 'parts/header.php'; ?>
    <div class="category-content container">
        <div class="row">
            <div class="col-12 breadcrumbs mb-4"> Casa, Gradina & Bricolaj / Mobilier & Saltele / Mobilier / Fotolii</div>
        </div>
        <div class="row">
            <div class="col-2 sidebar">
                <div class="sidebar-card">
                    <h6>Disponibilitate</h6>
                    <input type="checkbox" title="stoc" />In stoc <br>
                    <input type="checkbox" title="promotii" />Promotii <br>
                    <input type="checkbox" title="noutati" />Noutati <br>
                    <input type="checkbox" title="resigilate" />Resigilate <br>
                    <input type="checkbox" title="precomanda" />Precomanda <br>
                </div>
                <div class="sidebar-card">
                    <h6>Pret</h6>
                    <input type="checkbox" title="1" />1000-1500 <br>
                    <input type="checkbox" title="1" />1500-2000 <br>
                    <input type="checkbox" title="1" />2000-3000 <br>
                    <input type="checkbox" title="1" />3000-4000 <br>
                    <input type="checkbox" title="1" />4000-5000 <br>
                    <input type="checkbox" title="1" />Peste 5000 <br>
                    <hr />
                    <input type="checkbox" title="price">Interval pret <br>
                </div>
                <div class="sidebar-card">
                    <h6>Rating minim</h6>
                    <input type="checkbox" title="star" />&#x2605;&#x2605;&#x2605;&#x2605;&#x2605;<br>
                    <input type="checkbox" title="star" />&#x2605;&#x2605;&#x2605;&#x2605;&#x2606;<br>
                    <input type="checkbox" title="star" />&#x2605;&#x2605;&#x2605;&#x2606;&#x2606;<br>
                    <input type="checkbox" title="star" />&#x2605;&#x2605;&#x2606;&#x2606;&#x2606;<br>
                    <input type="checkbox" title="star" />&#x2605;&#x2606;&#x2606;&#x2606;&#x2606;<br>
                </div>
                <div class="sidebar-card">
                    <h6>Livrare prin Tazz by eMag</h6>
                    <label>Cauta...</label><br>
                    <input type="checkbox" title="delivery" />Bucuresti - Sector 1 <br>
                    <input type="checkbox" title="delivery" />Bucuresti - Sector 2 <br>
                    <input type="checkbox" title="delivery" />Bucuresti - Sector 3 <br>
                    <input type="checkbox" title="delivery" />Bucuresti - Sector 4 <br>
                    <input type="checkbox" title="delivery" />Bucuresti - Sector 5 <br>
                    <input type="checkbox" title="delivery" />Bucuresti - Sector 6 <br>
                </div>
                <div class="sidebar-card">
                    <h6>Disponibil prin easybox</h6>
                    <input type="checkbox" />Da <br>
                </div>
            </div>
            <div class="col-10 filters-product-list">
                <div class="row">
                    <div class="filters col-12">
                        <div class="mb-0 pb-0">
                            <span class="h5 font-weight-bold">Laptopuri&nbsp;&nbsp;&nbsp;</span><span style="font-size: 20px">1703 produse<br/></span>
                            <span style="font-size: 12px; background-color: orangered; color: #e7eff8">Nou</span><span>Prea multe optiuni?</span><span class="">Gaseste modelul potrivit<hr></span>
                        </div>
                        <div class="row mb-0 pb-0 mt-0 pt-0">
                            <div class="col-2 left mb-0 pb-0 mt-0 pt-0">
                                Brand:<br/>
                                <form class="left mb-0 pb-0 mt-0 pt-0" style="width: 100px">
                                    <select style="width: 150px;">
                                        <option value="Selecteaza...">Selecteaza...</option>
                                    </select>
                                </form>
                            </div>
                            <div class="col-2 left mb-0 pb-0 mt-0 pt-0">
                                Tip procesor:<br/>
                                <form style="width: 100px ">
                                    <select style="width: 150px;">
                                        <option value="Selecteaza...">Selecteaza...</option>
                                    </select>
                                </form>
                            </div>
                            <div class="col-2 mb-0 pb-0 mt-0 pt-0">
                                Tip laptop:<br/>
                                <form class="mt-0 pt-0 mt-0 pt-0" style="width: 100px ">
                                    <select style="width: 150px;">
                                        <option value="Selecteaza...">Selecteaza...</option>
                                    </select>
                                </form>
                            </div>
                        </div>
                        <div class="mb-0 pb-0 mt-0 pt-0"><hr></div>
                        <div class="row mb-0 pb-0 mt-0 pt-0">
                            <div class="col-3 col-lg-2 left mr-0 pr-0 ml-0 pl-0">Ordoneaza dupa:</div>
                            <div class="col-3 col-lg-2 mr-0 pr-0 ml-0 pl-0">
                                <form style="">
                                    <select>
                                        <option value="Cele mai populare">Cele mai populare</option>
                                    </select>
                                </form>
                            </div>
                            <div class="col-3 col-lg-2 mr-0 pr-0 ml-0 pl-0">Produse:</div>
                            <div class="col-3 col-lg-2 mr-0 pr-0 ml-0 pl-0">
                                <form style="">
                                    <select>
                                        <option value="60 pe pagina">60 pe pagina</option>
                                    </select>
                                </form>
                            </div>
                            <div class="d-none col-lg-4"></div>
                        </div>
                        <div><hr></div>
                    </div>
                </div>
                <div class="row product-list mt-0 pt-0 mb-0 pb-0">
                    <?php if(isset($products)):?>
                        <?php foreach($products as $product):?>
                            <div class="col-6 col-lg-4 col-xl-3">
                                <a href="product.php?product_id=<?php echo $product->id;?>"><?php product($product); ?></a>
                            </div>
                        <?php endforeach; ?>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
    <?php include 'parts/footer.html'; ?>
</body>
</html>